<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "popup".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property string $startDate
 * @property string $endDate
 * @property integer $tipo
 */
class Popup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'popup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'tipo'], 'integer'],
            [['startDate', 'endDate'], 'safe'],
            [['title', 'description'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Título',
            'description' => 'Contenido',
            'status' => 'Estado',
            'startDate' => 'Fecha Inicio',
            'endDate' => 'Fecha Fin',
            'tipo' => 'Tipo',
        ];
    }
}
