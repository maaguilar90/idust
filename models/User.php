<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\base\Security;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\filters\AccessControl;
use kartik\password\StrengthValidator;
use yii\models\Token;


/**
 * This is the model class for table "user".
 *
 * @property string $identity
 * @property string $type
 * @property string $address_home
 * @property string $phone_home
 * @property string $cellphone
 * @property string $cellphone_company
 * @property string $username
 * @property string $password
 * @property string $country_origin
 * @property string $province_residence
 * @property string $canton_residence
 * @property string $zone_residence
 * @property string $chief_representative
 * @property string $secondary_representative
 * @property string $status
 * @property integer $office
 * @property string $names
 * @property string $lastnames
 * @property string $economic_sector
 * @property integer $link
 * @property string $internal_rating
 * @property string $city_residence
 */
class User extends ActiveRecord implements IdentityInterface 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['identity', 'address_home', 'phone_home', 'cellphone', 'username', 'province_residence', 'canton_residence', 'zone_residence', 'names', 'lastnames', 'city_residence'], 'required'],
            [['office', 'link'], 'integer'],
            [['identity'], 'string', 'max' => 13],
            [['type', 'cellphone_company', 'status'], 'string', 'max' => 1],
            [['address_home'], 'string', 'max' => 80],
            [['phone_home'], 'string', 'max' => 20],
            [['cellphone', 'province_residence', 'canton_residence', 'zone_residence', 'chief_representative', 'secondary_representative', 'economic_sector', 'internal_rating'], 'string', 'max' => 10],
            [['username'], 'string', 'max' => 150],
            [['password'], 'string', 'max' => 255],
            [['country_origin'], 'string', 'max' => 40],
            ['type_client', 'in', 'range' => ['CLIENT','ADMIN']],
            ['status_client', 'in', 'range' => ['ACTIVE','INACTIVE']],
            [['names', 'lastnames', 'city_residence'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'identity' => 'Cédula',
            'type' => 'Tipo',
            'address_home' => 'Dirección Casa',
            'phone_home' => 'Teléfono Casa',
            'cellphone' => 'Celular',
            'cellphone_company' => 'Celular Empresa',
            'username' => 'Email',
            'password' => 'Password',
            'country_origin' => 'País de Origen',
            'province_residence' => 'Provincia de Residencia',
            'canton_residence' => 'Cantón de Residencia',
            'zone_residence' => 'Zona de Residencia (Parroquia)',
            'chief_representative' => 'Representante Legal',
            'secondary_representative' => 'Representante Secundario',
            'status' => 'Estatus',
            'office' => 'Oficina',
            'names' => 'Nombres',
            'lastnames' => 'Apellidos',
            'economic_sector' => 'Sector Económico',
            'link' => 'Link',
            'internal_rating' => 'Internal Rating',
            'city_residence' => 'Ciudad de Residencia',
        ];
    }
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
        public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" No está implementado.');
    }
        public function beforeSave($insert) {

         if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
                  if(isset($this->password)) 
                    $this->password = $this->hashPassword($this->password);
            }
        }
        return parent::beforeSave($insert);
    }
            public static function isUserClient($username)
    {
      if (static::findOne(['username' => $username, 'type_client' => 'CLIENT'])){
 
             return true;
      } else {
 
             return false;
      }
 
    }
        public static function isUserAdmin($username)
    {
      if (static::findOne(['username' => $username, 'type_client' => 'ADMIN'])){
 
             return true;
      } else {
 
             return false;
      }
 
    }
        public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
        public static function findByPasswordResetToken($token)
    {
        $expire = (1 * 4 * 60 * 60);
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }
         public function getId()
    {
        return $this->identity;
    }

    public function getUsername()
    {
        return $this->username;
    }
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }
    public function validatePassword($password)
    {
         return $this->password === md5($password);
    }
    public function hashPassword($password){

        //return hash('sha256',$password);
        return md5($password);
    }
    
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = \Yii::$app->getSecurity()->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    } 
}
