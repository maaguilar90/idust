<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\db\Query;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
         return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        //die("dd");
       

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
            return $this->redirect('/web/admin');

        }else{

             if (Yii::$app->user->isGuest) {
                 return $this->render('login', [
                    'model' => $model,
                ]);
                //die("ff22");
            //return $this->redirect(Yii::$app->user->loginUrl);
            }else{
                return $this->redirect('/web/admin');
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->user->loginUrl);
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            // change layout for error action
            if ($action->id == 'login')
                $this->layout = 'login';
            return true;
        } else {
            return false;
        }
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

        public function actionCesantia()
    {
        $model = new ContactForm();
       
           // Yii::$app->session->setFlash('contactFormSubmitted');

            //return $this->refresh();
       
        return $this->render('cesantia', [
            'model' => $model,
        ]);
    }

 

    /**
     * Displays Consulta cupon cedula.
     *
     * @return string
     */
    public function actionConsultacuponcodigo()
    {
        $result = array();
        $return['status'] = false;
        if (!empty($_POST['param'])) {
            $tabla = 'opd_coupon_transaction_zombies';
            $return['status'] = true;
            $return['data'] = true;
            $arr_result = array();
            $arr_result_land = array();

            $query = new Query();
            $query->select('id, coupon_number,local_code_emission,terminal_code,ticket,status,creation_date')
                ->from($tabla . ' as a')
                ->where('coupon_number = ' . $_POST['param']);
            $command = $query->createCommand();
            $data = $command->queryAll();

            if ($data) {
                foreach ($data as $item) {
                    $arr_result['coupon_number'] = $item['coupon_number'];
                    $arr_result['local_code_emission'] = $item['local_code_emission'];
                    $arr_result['terminal_code'] = $item['terminal_code'];
                    $arr_result['ticket'] = $item['ticket'];
                    $arr_result['status'] = $item['status'];
                    $arr_result['creation_date'] = $item['creation_date'];
                }

                $query = new Query();
                $query->select('a.identity, a.codigo, a.sucursal, a.caja, a.ticket, a.num_tajeta_mas, a.tiene_si_no, a.date_create, b.name, b.lastname, b.email, b.province,p.description, b.city, b.address, b.phone, b.creation_date, b.birthdate, b.cellphone, b.gender, b.tarjetamas, b.terminos')
                    ->from('user_zombie_ticket_2017 as a')
                    ->innerJoin('user_zombies_2017 as b', 'a.identity = b.identity')
                    ->innerJoin('province as p', 'b.province = p.id')
                    ->where("a.codigo = '" . $_POST['param'] . "'")
                    ->groupBy('a.codigo');
                $command = $query->createCommand();
                //echo $command->getRawSql();
                //die();
                $data_landing = $command->queryAll();

                if ($data_landing) {
                    foreach ($data_landing as $item) {
                        $arr_result_land['identity'] = $item['identity'];
                        $arr_result_land['codigo'] = $item['codigo'];
                        $arr_result_land['sucursal'] = $item['sucursal'];
                        $arr_result_land['caja'] = $item['caja'];
                        $arr_result_land['ticket'] = $item['ticket'];
                        $arr_result_land['num_tajeta_mas'] = $item['num_tajeta_mas'];
                        $arr_result_land['tiene_si_no'] = $item['tiene_si_no'];
                        //$arr_result_land['date_create'] = $item['date_create'];
                        $arr_result_land['name'] = $item['name'];
                        $arr_result_land['lastname'] = $item['lastname'];
                        $arr_result_land['email'] = $item['email'];
                        $arr_result_land['province'] = $item['province'];
                        $arr_result_land['description'] = $item['description'];
                        $arr_result_land['city'] = $item['city'];
                        $arr_result_land['address'] = $item['address'];
                        $arr_result_land['phone'] = $item['phone'];
                        $arr_result_land['creation_date'] = $item['creation_date'];
                        $arr_result_land['birthdate'] = $item['birthdate'];
                        $arr_result_land['cellphone'] = $item['cellphone'];
                        $arr_result_land['gender'] = $item['gender'];
                        $arr_result_land['tarjetamas'] = $item['tarjetamas'];
                        $arr_result_land['terminos'] = $item['terminos'];
                    }
                    $return['status_landing'] = true;
                } else {
                    $return['status_landing'] = false;
                }
                $return['status_integracion'] = true;
                $return['base_integracion'] = $arr_result;
                $return['base_landing'] = $arr_result_land;
                $return['status'] = true;
            } else {
                $return['status_integracion'] = false;
                $return['status_landing'] = false;
                $return['base_integracion'] = $arr_result;
                $return['base_landing'] = $arr_result_land;
            }


        }

        echo json_encode($return);
    }

    /**
     * Consulta cupones Zombies.
     *
     * @return string
     */
    public function actionZcupones()
    {
        return $this->render('zcupones');
    }

    /**
     * Reporte trivia oct 2017 .
     *
     * @return string
     */
 

    /**
     * Reporte look oct 2017 .
     *
     * @return string
     */
    public function actionLook()
    {
        //------ Tablas Landing ---------------------------
        $tablaReg = 'user_look_2017';
        $tablaClickShare = 'user_look_click_face_2017';
        $tablaTicket = 'user_look_ticket_2017';

        //------------------------------------------------TOTALES------------------------------------------------
        //------ Total Registros -------------------------
        $query = new Query();
        $query->select('COUNT(DISTINCT identity) as total')
            ->from($tablaReg . ' as a')
            ->limit(1);
        $command = $query->createCommand();
        $modelReg = $command->queryAll();
        //------ Total Compartido ------------------------
        $query = new Query();
        $query->select('COUNT(DISTINCT a.identity) as total')
            ->from($tablaClickShare . ' as a')
            ->innerJoin($tablaReg . ' as b', 'a.identity=b.identity')
            ->limit(1);
        $command = $query->createCommand();
        $modelComp = $command->queryAll();
        //------ Total Compartido ------------------------
        $query = new Query();
        $query->select("COUNT(DISTINCT `identity`, `codigo`, `sucursal`, `caja`, `ticket` ) AS total")
            ->from($tablaTicket);
        $command = $query->createCommand();
        $modelCupon = $command->queryAll();

        //------------------------------------------------ X DIA ------------------------------------------------
        //------ Registro por Dia ------------------------
        $query = new Query();
        $query->select("DATE(`creation_date`) AS fecha, COUNT(DISTINCT `identity`) AS cant")
            ->from($tablaReg)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regXdia = $command->queryAll();
        //------ Ticket por Dia ------------------------
        $query = new Query();
        $query->select("DATE(`date_create`) AS fecha, COUNT(DISTINCT `identity`) AS cant")
            ->from($tablaTicket)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regTkdia = $command->queryAll();
        //------ Ticket por Dia ------------------------
        $query = new Query();
        $query->select("DATE(date_create) AS fecha, COUNT(DISTINCT identity) AS cant")
            ->from($tablaClickShare)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regCompDia = $command->queryAll();

        return $this->render('look', [
            'link' => 'look',
            'title' => 'Reporte | Tía - Look que inspira',
            'totalReg' => $modelReg[0]['total'],
            'totalComp' => $modelComp[0]['total'],
            'totalCupon' => $modelCupon[0]['total'],
            'regPorDia' => $regXdia,
            'regTkDia' => $regTkdia,
            'regCompDia' => $regCompDia,
        ]);
    }

    /**
     * Reporte Zombies oct 2017 .
     *
     * @return string
     */
    public function actionZombies()
    {
        //------ Tablas Landing ---------------------------
        $landing = 'zombies';
        $tablaReg = 'user_zombies_2017';
        $tablaClickShare = 'user_zombie_click_face_2017';
        $tablaTicket = 'user_zombie_ticket_2017';
        $tablaTurnos = 'user_zombie_turno_2017';

        //------------------------------------------------TOTALES------------------------------------------------
        //------ Total Registros -------------------------
        $query = new Query();
        $query->select('COUNT(DISTINCT identity) as total')
            ->from($tablaReg . ' as a')
            ->limit(1);
        $command = $query->createCommand();
        $modelReg = $command->queryAll();
        //------ Total Compartido ------------------------
        $query = new Query();
        $query->select('COUNT(DISTINCT a.identity) as total')
            ->from($tablaClickShare . ' as a')
            ->innerJoin($tablaReg . ' as b', 'a.identity=b.identity')
            ->limit(1);
        $command = $query->createCommand();
        $modelComp = $command->queryAll();
        //------ Total Compartido ------------------------
        $query = new Query();
        $query->select("COUNT(DISTINCT `identity`, `codigo`, `sucursal`, `caja`, `ticket` ) AS total")
            ->from($tablaTicket);
        $command = $query->createCommand();
        $modelCupon = $command->queryAll();
        //------ Total Jugado ------------------------
        $query = new Query();
        $query->select("COUNT(0) AS total")
            ->from($tablaTurnos)
            ->where("resultado <> 'PENDIENTE'");
        $command = $query->createCommand();
        $modelJuego = $command->queryAll();

        //------------------------------------------------ X DIA ------------------------------------------------
        //------ Registro por Dia ------------------------
        $query = new Query();
        $query->select("DATE(`creation_date`) AS fecha, COUNT(DISTINCT `identity`) AS cant")
            ->from($tablaReg)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regXdia = $command->queryAll();
        //------ Ticket por Dia ------------------------
        $query = new Query();
        $query->select("DATE(`date_create`) AS fecha, COUNT(DISTINCT `identity`) AS cant")
            ->from($tablaTicket)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regTkdia = $command->queryAll();
        //------ Ticket por Dia ------------------------
        $query = new Query();
        $query->select("DATE(date_create) AS fecha, COUNT(DISTINCT identity) AS cant")
            ->from($tablaClickShare)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regCompDia = $command->queryAll();

        return $this->render('zombies', [
            'link' => 'zombies',
            'title' => 'Reporte | Tía - Invasion Zombie',
            'totalReg' => $modelReg[0]['total'],
            'totalComp' => $modelComp[0]['total'],
            'totalCupon' => $modelCupon[0]['total'],
            'totalJugado' => $modelJuego[0]['total'],
            'regPorDia' => $regXdia,
            'regTkDia' => $regTkdia,
            'regCompDia' => $regCompDia,
        ]);
    }

    /**
     * Reporte Ticket Regalon oct 2017 .
     *
     * @return string
     */
    public function actionTicket()
    {
        //------ Tablas Landing ---------------------------
        $landing = 'billeton';
        $tablaReg = 'user_billeton_2017';
        $tablaClickShare = 'user_billeton_click_face_2017';
        $tablaTicket = 'user_billeton_ticket_2017';
        $tablaTurnos = 'user_billeton_turno_2017';

        //------------------------------------------------TOTALES------------------------------------------------
        //------ Total Registros -------------------------
        $query = new Query();
        $query->select('COUNT(DISTINCT identity) as total')
            ->from($tablaReg . ' as a')
            ->limit(1);
        $command = $query->createCommand();
        $modelReg = $command->queryAll();
        //------ Total Compartido ------------------------
        $query = new Query();
        $query->select('COUNT(DISTINCT a.identity) as total')
            ->from($tablaClickShare . ' as a')
            ->innerJoin($tablaReg . ' as b', 'a.identity=b.identity')
            ->limit(1);
        $command = $query->createCommand();
        $modelComp = $command->queryAll();
        //------ Total Compartido ------------------------
        $query = new Query();
        $query->select("COUNT(DISTINCT `identity`, `codigo`, `sucursal`, `caja`, `ticket` ) AS total")
            ->from($tablaTicket);
        $command = $query->createCommand();
        $modelCupon = $command->queryAll();
        //------ Total Jugado ------------------------
        $query = new Query();
        $query->select("COUNT(0) AS total")
            ->from($tablaTurnos)
            ->where("resultado <> 'PENDIENTE'");
        $command = $query->createCommand();
        $modelJuego = $command->queryAll();

        //------------------------------------------------ X DIA ------------------------------------------------
        //------ Registro por Dia ------------------------
        $query = new Query();
        $query->select("DATE(`creation_date`) AS fecha, COUNT(DISTINCT `identity`) AS cant")
            ->from($tablaReg)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regXdia = $command->queryAll();
        //------ Ticket por Dia ------------------------
        $query = new Query();
        $query->select("DATE(`date_create`) AS fecha, COUNT(DISTINCT `identity`) AS cant")
            ->from($tablaTicket)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regTkdia = $command->queryAll();
        //------ Ticket por Dia ------------------------
        $query = new Query();
        $query->select("DATE(date_create) AS fecha, COUNT(DISTINCT identity) AS cant")
            ->from($tablaClickShare)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regCompDia = $command->queryAll();

        return $this->render('ticket', [
            'link' => 'ticket',
            'title' => 'Reporte | Tía - Ticket Regalón',
            'totalReg' => $modelReg[0]['total'],
            'totalComp' => $modelComp[0]['total'],
            'totalCupon' => $modelCupon[0]['total'],
            'totalJugado' => $modelJuego[0]['total'],
            'regPorDia' => $regXdia,
            'regTkDia' => $regTkdia,
            'regCompDia' => $regCompDia,
        ]);
    }

}
