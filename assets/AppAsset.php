<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/style-asoprep.css',
        'css/queries-asoprep.css',
        'css/bootstrap.css',
        'css/slick.css',
        'css/slick-theme.css',
    ];
    public $js = [
        'js/jquery.js',
        'js/3dcarousel.js',
        //'js/bootstrap.js',
        'js/slick.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
