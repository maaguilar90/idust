<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'iDust';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	$('.slider-carousel').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
    {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
    },
    {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
    },
    {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });
  $('.slider-carousel2').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
    {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
    },
    {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
    },
    {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });


});

$(document).ready(function(){
      $('#carousel').carousel3d();
    });

JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>



<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


    
<section class="container-fluid">
	<div class="fotorama" data-autoplay="true">
        <?php $sliders = Slider::find()->OrderBy('orden')->all(); ?>
        <?php foreach ($sliders as $slider): ?>

		<!--<div data-img="<?= URL::base() ?>/images/banner/<?= $slider->image ?>"><a href="<?= $slider->description ?>"></a></div>-->
        <?php endforeach ?>


	</div>
  <div class="container-redes">
    <div><img src="<?= URL::base() ?>/images/site/facebook_icon.svg" class="redes-lateral"></div>
    <div><img src="<?= URL::base() ?>/images/site/twitter_icon.svg" class="redes-lateral"></div>
    <div><img src="<?= URL::base() ?>/images/site/instagram_icon.svg" class="redes-lateral"></div>
    <div><img src="<?= URL::base() ?>/images/site/whatsapp_icon.svg" class="redes-lateral"></div>
    <div><img src="<?= URL::base() ?>/images/site/youtube_icon.svg" class="redes-lateral"></div>
    
  </div>
  <div id="container" style="height: 610px; padding-top: 4%; background-color: #EEE;"  class="fotorama-carousel">
     <div id="perspective">
      <div id="carousel">
        <!--<figure> <img src="<?= URL::base() ?>/images/site/ruleta1.png" alt="fotorama1" style="width: 300px"></figure>-->

        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta01.fw.png" alt="fotorama1" style="width: 300px">
          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta02.fw.png" alt="fotorama1" style="width: 300px">
          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta03.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta04.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta05.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta06.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta07.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <div class="div-carousel">
              <span class="span-carousel">&nbsp</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>
            <img src="<?= URL::base() ?>/images/site/ruleta1.png" alt="fotorama1" style="width: 300px">
          </div>
        </figure>
      </div>
    </div>

    <button class="prev"> < </button>
    <button class="next"> > </button>
  </div>

</section>
        <!-- --> 


<section class="container-fluid" style="margin-top: -5.5%; background-color: black;">
    <div class="servicio">
        <div class="servicio-int" style="border: none;">
          <a href="#sectabout">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" class="btn1" alt="servicio1" class="" style="">
          </a>
        </div>
        <div class="servicio-int">
          <a href="#sectcleaning">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" class="btn2" alt="servicio1" class="" style="">
          </a>
        </div>
        <div class="servicio-int">
          <a href="#sectinsured">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" class="btn3" alt="servicio1" class="" style="">
          </a>
        </div>
        <div class="servicio-int">
          <a href="javascript:openForm();">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" class="btn4" alt="servicio1" class="" style="">
          </a>
        </div>
        <div class="servicio-int">
          <a href="#sectcontact">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" class="btn5" alt="servicio1" class="" style="">
          </a>
        </div>
    </div>
</section>

<section class="container-fluid" id="sectabout">
    <div class="cleaning-services" style="">
      <div class="cleaning-left">
        <div class="bloque">
          <div class="label2">
              <img class="imgsuper" src="<?= URL::base() ?>/images/site/aboutus.jpg"  />
          </div>

          <div class="label1 tjustify">
              <div class="column1 tcenter">
                <span class="title1">ABOUT</span><span class="title2">US</span><br><br>
              </div>
              <span>Founded in the spring of 2014, in the heart of New York City, iDust Cleaning Services Corp is a cleaning company with a reputation for excellence that has specialized in the restaurant and hotel industry sector, growing with our clients, always taking care of their investment and providing the demand they need..</span>
          </div>
          
        </div>
        <div class="bloque">
          <div class="label1 tjustify">
              <div class="column1 tcenter">
                <br><br>
              </div>
              <span>Founded in the spring of 2014, in the heart of New York City, iDust Cleaning Services Corp is a cleaning company with a reputation for excellence that has specialized in the restaurant and hotel industry sector, growing with our clients, always taking care of their investment and providing the demand they need..</span>
          </div>
          <div class="label2 right">
              <img class="imgsuper right" src="<?= URL::base() ?>/images/site/aboutus.jpg"  />
          </div>
        </div>
      </div>
      
    </div>
</section>
<section class="container-fluid"  id="sectwork">
  <div class="workwith" style="">
    <div class="work-left">
      <img class="imgsuper image" src="<?= URL::base() ?>/images/site/imgtrabaja.jpg"  />
    </div>
    <div class="work-right">
      <div class="column1 tcenter">
        <span class="title1">WORK WITH</span><span class="title2">US</span><br><br>
        <br><br>
        <button class="button">REGISTER NOW</button>
      </div>
    </div>
  </div>
</section>

<section class="container-fluid"  id="sectcleaning">
    <div class="how-works">
      <div class="cabecera"><img class="" src="<?= URL::base() ?>/images/site/top-works.fw.png"  /></div>
      <div class="contenedor">
        <div style="height: 10px;"></div>
        <div class="seccion">
          <div class="titlework tright"><span class="text1">CLEANING</span><span class="text2"> SERVICES</span></div>
          <div class="slider-carousel" style="padding-left: 2.5%;">
            <div>
              <a href="javascript: openPopup(1);">
                <div class="title-slider">
                  <span>Kitchen</span>
                </div>
                <img class="" src="<?= URL::base() ?>/images/site/sliderwork1.jpg"  />
                <br>
                <div class="text-slider">
                  <span>Care on the surfaces of each corner of the kitchen with a special polish for the metal, makes our service the best!</span>
                </div>
              </a>
            </div>
            <div>
              <a href="javascript: openPopup(2);">
                <div class="title-slider">
                  <span>Hotline Equipement</span>
                </div>
                <img class="" src="<?= URL::base() ?>/images/site/sliderwork2.jpg"  />
                <br>
                <div class="text-slider">
                  <span>Specialists in high temperature equipment, we make sure that the processes managed and supervised by our team are with the best recommendations of the manufacturers.</span>
                </div>
              </a>
            </div>
            <div>
              <a href="javascript: openPopup(3);">
                <div class="title-slider">
                  <span>Dining Room</span>
                </div>
                <img class="" src="<?= URL::base() ?>/images/site/sliderwork7.jpg"  />
                <br>
                <div class="text-slider">
                  <span>The details make your place memorable, rely on the total care of your space with the experience of our team</span>
                </div>
              </a>
            </div>
            <div>
              <a href="javascript: openPopup(4);">
                <div class="title-slider">
                  <span>High Dustin</span>
                </div>
                <img class="" src="<?= URL::base() ?>/images/site/sliderwork6.jpg"  />
                <br>
                <div class="text-slider">
                  <span>it appears everywhere but we can keep its place without it</span>
                </div>
              </a>
            </div>
            <div>
              <a href="javascript: openPopup(5);">
                <div class="title-slider">
                  <span>Bar / Night Club</span>
                </div>
                <img class="" src="<?= URL::base() ?>/images/site/sliderwork4.jpg"  />
                <br>
                <div class="text-slider">
                  <span>The bar counter will be displayed on your best night with our care.</span>
                </div>
              </a>
            </div>
             
          </div>
        </div>
      </div>
    </div>
</section>

<section class="container-fluid" style="margin-top: 3%;" id="sectinsured">
    <div class="aboutas">
        <div class="title-cl">
          <div class="column1">&nbsp;</div>
          <div class="column2 tright"><span class="title1">INSURED</span><span class="title2"></span><br></div>
          
        </div>
        <div class="bloque">
          <div class="label1">
              <img class="imgsuper" src="<?= URL::base() ?>/images/site/insured.jpg"  />
          </div>
          <div class="label2 tjustify">
              <!--<span class="titlelabel">Who We Are</span><br>
              <span class="titlelabel">GET TO KNOW US</span><br><br>-->
              <span>Supervisors ensure that employees are taught equipment and chemical safety, safe work procedures and best practices in daily work routines as well as the importance of protecting working area contents and personal belongings. 
              idust Cleaning Services Corp complies with all relevant Federal, State, and local codes and all other ordinances, regulations, etc. that are applicable to the work. Additionally, we are fully insured, with a combined $5 million General Liability umbrella policy.</span>
          </div>
        </div>
    </div>
</section>

<section class="container-fluid"  id="sectour">
    <div class="how-works">
      <div class="cabecera"><img class="" src="<?= URL::base() ?>/images/site/top-works.fw.png"/></div>
      <div class="contenedor">
        <div style="height: 20px;"></div>
        <div class="seccion">
          <div class="titlework tright"><span class="text1">OUR</span><span class="text2">CLIENTS</span></div>
          <div class="slider-carousel2" style="padding-left: 1%; margin-right: 1%; margin-top: 2%;">
            <!--<div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa1.fw.png"/></div>
            <div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa2.fw.png"/></div>
            <div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa3.fw.png"/></div>
            <div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa1.fw.png"/></div>
            <div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa2.fw.png"/></div>
            <div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa2.fw.png"/></div>
            <div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa2.fw.png"/></div>
            <div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa2.fw.png"/></div>
            <div><img class="img-slider2" src="<?= URL::base() ?>/images/site/empresa2.fw.png"/></div>-->
            <div style="text-align: center; border-right: 2px solid #b8b8b8;"> <span style="color: #b8b8b8; font-size: 18px;">SOHO </span></div>
            <div style="text-align: center; border-right: 2px solid #b8b8b8;"> <span style="color: #b8b8b8; font-size: 18px;">TRIBECA </span></div>
            <div style="text-align: center; border-right: 2px solid #b8b8b8;"> <span style="color: #b8b8b8; font-size: 18px;">NOHO </span></div>
            <div style="text-align: center; border-right: 2px solid #b8b8b8;"> <span style="color: #b8b8b8; font-size: 18px;">GREENWICH VILLAGE </span></div>
            <div style="text-align: center; border-right: 2px solid #b8b8b8;"> <span style="color: #b8b8b8; font-size: 18px;">LOWER EAST </span></div>
            <div style="text-align: center; border-right: 2px solid #b8b8b8;"> <span style="color: #b8b8b8; font-size: 18px;">MIDTOWN EAST </span></div>
          </div>
         <!-- <div style="text-align: center;">
           
            <span style="color: #b8b8b8; font-size: 20px;">TRIBECA |</span>
            <span style="color: #b8b8b8; font-size: 20px;">NOHO |</span>
            <span style="color: #b8b8b8; font-size: 20px;">GREENWICH VILLAGE |</span>
            <span style="color: #b8b8b8; font-size: 20px;">LOWER EAST |</span>
            <span style="color: #b8b8b8; font-size: 20px;">MIDTOWN EAST</span>
          </div>-->
        </div>
      </div>
    </div>
</section>

<section class="container-fluid" style="margin-top: 3%;"  id="sectcontact">
    <div class="contactus">
        <div class="title-cl">
          <div class="column1">&nbsp;</div>
          <div class="column2 tright"><span class="title1">CONTACT</span><span class="title2">US</span><br></div>
          
        </div>
        <div class="bloque">
          <div class="label1">
              <img class="imgtrabaja" src="<?= URL::base() ?>/images/site/contactus.svg"  />
              <img class="imgsuper" src="<?= URL::base() ?>/images/site/contactus.fw.png"  />
          </div>
          <div class="label2 tjustify">
            <div>
                <div style="vertical-align: middle;">
                    <img style="width: 4.8%;" src="<?= URL::base() ?>/images/site/horario.svg"/>
                    <span class="titleencabezado" style="vertical-align: middle;">Schedule Office:</span>
                </div>
                <span style="padding: 24px;">Mon - Sat: 09:00 - 17:00</span><br>
                <div style="vertical-align: middle;">
                    <img style="width: 4.8%;" src="<?= URL::base() ?>/images/site/horario.svg"/>
                    <span class="titleencabezado" style="vertical-align: middle;">Schedule Service:</span>
                </div>
                <span style="padding: 24px;">Mon - Sun: 23:30 - 08:00</span><br>
            </div>
            <div class="tright">
                <div style="vertical-align: middle;">
                    <img style="width: 4.8%;" src="<?= URL::base() ?>/images/site/ubicacion.svg"/>
                    <span class="titleencabezado" style="vertical-align: middle;">Address:</alto dustinspan>
                </div>
                <span style="padding: 0px;">507 W 186th ST New York, NY, 10033</span><br>
            </div>
            <div>
                <div style="vertical-align: middle;">
                    <img style="width: 4.8%;" src="<?= URL::base() ?>/images/site/ubicacion.svg"/>
                    <span class="titleencabezado" style="vertical-align: middle;">Phone:</span>
                </div>
                <span style="padding: 24px;">(347) 403-2548</span><br>
            </div>
            <div class="tcenter">
                <div style="vertical-align: middle;">
                    <span class="titleencabezado" style="vertical-align: middle;">Email:</span>
                </div>
                <span style="padding: 24px;">info@idustnyc.com</span><br>
            </div>
          </div>
        </div>
    </div>
</section>

<?php $popups = Popup::find()->all(); ?>
        <?php foreach ($popups as $popup): ?>
        <?php $popup=$popup->description ?>
        <?php $tipo=$popup->description ?>
        <?php 
            $contenido='<img src="'.$popup.'" width="98%" />'  

        ?>
        <?php endforeach ?>


<?php 
    if(@$info){
?>
    <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content" style="height: 23%;    overflow-y: auto;">
   
    <span class="close">x</span>
    <div id="contentmodal" style="margin-top:30px;">
        <?=@$infocontent ?>
    </div>
  </div>

</div>
<?php

    }else{
         if(@$popup){
?>
    <div id="myModal" class="modal" style="    overflow-y: hidden;    z-index: 2000;">

  <!-- Modal content -->
  <div class="modal-content" style="    z-index: 2000; height: 85;    overflow-y: auto;     width: 35%;     padding: 2px;    margin: 2% auto;">
   
    <span class="close">x</span>
    <div id="contentmodal" style="margin-top:10px;">
        <?php
           
                echo $contenido;
            
        ?>
        
    </div>
  </div>

</div>
<?php
        }
    }
?>


 <div id="myModal2" class="modal" style="    overflow-y: hidden;    z-index: 2000;">
  <!-- Modal content -->
  <div class="modal-content" style="    z-index: 2000; height: 85;    overflow-y: auto;     width: 65%;     padding: 2px;    margin: 4% auto; background-color: transparent; border: none;-webkit-box-shadow: none; box-shadow: none;">
    <div id="contentmodal2" style="margin-top:0px;">
      <section class="container-fluid">
        <div class="popup-content" style="padding-left: 0px; padding-right: 0px; padding-top: 0px; padding-bottom: 0px;">
          <div class="cabecera"><img class="" src="<?= URL::base() ?>/images/site/top-works.fw.png"/></div>
          <div class="contenedor">
            <div style="height: 30px;">
              <div style="text-align: right;"><span class="close2">x</span></div>
            </div>
            <div class="seccion">
              <div class="titlework tright"><span class="text1" id="titlepopup">KITCHEN</span><span class="text2"></span></div>
              <div class="column1" style="padding-left: 1%;">
                <div><img class="img-slider2" id="imagepopup" src="<?= URL::base() ?>/images/site/sliderwork1.fw.png"/></div>
              </div>
              <div class="column2" style="padding-left: 4%;">
                <div><span id="contentpopup">Compañía dedicada a la limpieza industrial de restaurants específicamente el área de la cocina y los salones como algo general. Los servicios únicamente se hacen por la noche, comúnmente desde la media noche hasta 7 am que empieza a llegar la gente de los lugares.
En nueva york es muy importante que las compañías tengan todos los SEGUROS y que la calidad de servicio sea tal que permita una calificación A de SANIDAD</span></div>
                    <div class="items">
                        <div class="it-col1">
                          <img class="img-item" src="<?= URL::base() ?>/images/site/tiempo.svg"/>
                          <br><span>Make an<br> appointment</span>
                        </div>
                        <div class="it-col2">
                          <img class="img-item" src="<?= URL::base() ?>/images/site/localizacion.svg"/>
                          <br><span>Review <br>your Place</span>
                        </div>
                        <div class="it-col3">
                          <img class="img-item" src="<?= URL::base() ?>/images/site/telcontact.svg"/>
                          <br><span>Contact <br>Us</span>
                        </div>
                    </div>
              </div>
              
            </div>
          </div>
        </div>
    </section>
    </div>
  </div>
</div>

<div id="myModal3" class="modal" style="    overflow-y: hidden;    z-index: 2000;">
  <!-- Modal content -->
  <div class="modal-content" style="    z-index: 2000; height: 85;    overflow-y: auto;     width: 45%;     padding: 2px;    margin: 4% auto; background-color: transparent; border: none;-webkit-box-shadow: none; box-shadow: none;">
    
    <div id="contentmodal3" style="margin-top:0px;">

      <section class="container-fluid">
        <div class="popup-content" style="padding-left: 0px; padding-right: 0px; padding-top: 0px; padding-bottom: 0px;">

          <div class="cabecera"><img class="" src="<?= URL::base() ?>/images/site/top-works.fw.png"/></div>
          <div class="contenedor" style="margin-top: -0.5%;">

            <div style="height: 30px;">
              <div style="text-align: right;"><span class="close3">x</span></div>
            </div>
            <div class="seccion" style="color: white; padding-top: 10px">
              <div style="text-align: center;"><span style="font-size: 25px;">GET A QUOTE FORM</span></div>
              <div style="text-align: center;">
                <table>
                  <tr>
                    <td>First Name:</td>
                    <td><input type="" name="" style="height: 25px; width: 250px;"></td>
                  </tr>
                  <tr>
                    <td>Last Name:</td>
                    <td><input type="" name="" style="height: 25px; width: 250px;"></td>
                  </tr>
                  <tr>
                    <td>Email:</td>
                    <td><input type="" name="" style="height: 25px; width: 250px;"></td>
                  </tr>
                  <tr>
                    <td>Phone:</td>
                    <td><input type="" name="" style="height: 25px; width: 250px;"></td>
                  </tr>
                  <tr>
                    <td>Company:</td>
                    <td><input type="" name="" style="height: 25px; width: 250px;"></td>
                  </tr>
                  <tr>
                    <td>Company Address:</td>
                    <td><input type="" name="" style="height: 25px; width: 250px;"></td>
                  </tr>
                </table>
                <div style="padding-top: 10px;"> <button style="background-color: #EEB618; padding: 4px 20px 4px 20px; border: none; "> Send</button></div>
              </div>
                
            </div>
          </div>
        </div>
    </section>
    </div>
  </div>
</div>

<style type="text/css">
.seccion
{

}

.seccion table
{
  text-align: center;
  width: 85%;
}

.seccion table tr td
{
  text-align: right;
  padding: 3px;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    font-size: 11px;
    font-weight: bold;
    /* background: black; */
    /* padding-left: 5px; */
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
    color: red;
    padding-top: 2px;
    padding-bottom: 3px;
    font-size: 23px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}

.close2 {
    color: white;
    font-size: 11px;
    font-weight: bold;
    /* background: black; */
    /* padding-left: 5px; */
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
    color: red;
    padding-top: 2px;
    padding-bottom: 3px;
    font-size: 23px;
}

.close2:hover,
.close2:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}

.close3 {
    color: white;
    font-size: 11px;
    font-weight: bold;
    /* background: black; */
    /* padding-left: 5px; */
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
    color: red;
    padding-top: 2px;
    padding-bottom: 3px;
    font-size: 23px;
}

.close3:hover,
.close3:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<style type="text/css">
    #perspective {
      margin: 0 auto;
      margin-top: 0px;
      width: 500px;
      /*height: 140px;*/
      text-align: center;
      padding-left: 7%;
      position: relative;
      -webkit-perspective: 1300px;
      perspective: 1300px;
    }

    #carousel {
      width: 100%;
      height: 100%;
      position: absolute;
      -webkit-transform-style: preserve-3d;
      -moz-transform-style: preserve-3d;
      -webkit-transform: rotateY(0deg) translateZ(-288px);
    }

    #carousel figure {
      display: block;
      position: absolute;
      /*background: #399294;*/
      width: 296px;
      height: 106px;
      opacity: 0.9;
      margin: 12px;
      color: #fff;
      cursor: pointer;
      -webkit-transition: opacity 1s, -webkit-transform 1s;
             -moz-transition: opacity 1s, -moz-transform 1s;
               -o-transition: opacity 1s, -o-transform 1s;
                  transition: opacity 1s, transform 1s;
    }


  </style>

  <script type="text/javascript">

    function openPopup(id)
    {
      var title="";
      var image="";
      var description="";
      if (id==1){
        title="KITCHEN";
        image="<?= URL::base() ?>/images/site/sliderwork1.jpg";
        description="Care on the surfaces of each corner of the kitchen with a special polish for the metal, makes our service the best!";
      }

      if (id==2){
        title="HOTLINE EQUIPEMENT";
        image="<?= URL::base() ?>/images/site/sliderwork2.jpg";
        description="Specialists in high temperature equipment, we make sure that the processes managed and supervised by our team are with the best recommendations of the manufacturers.";
      }

      if (id==3){
        title="DINING ROOM";
        image="<?= URL::base() ?>/images/site/sliderwork7.jpg";
        description="The details make your place memorable, rely on the total care of your space with the experience of our team.";
      }

      if (id==4){
        title="HIGH DUSTIN";
        image="<?= URL::base() ?>/images/site/sliderwork6.jpg";
        description="it appears everywhere but we can keep its place without it.";
      }

      if (id==5){
        title="BAR / NIGHT CLUB";
        image="<?= URL::base() ?>/images/site/sliderwork6.jpg";
        description="The bar counter will be displayed on your best night with our care..";
      }      
      document.getElementById("titlepopup").innerHTML =title;
      document.getElementById("imagepopup").src=image;
      document.getElementById("contentpopup").innerHTML =description;

      var modal2 = document.getElementById('myModal2');
      modal2.style.display = "block";
    }


    //document.getElementById('myModal2').style.display="block"; 
    var modal2 = document.getElementById('myModal2');
    var btn2 = document.getElementById("myBtn2");

    // Get the <span> element that closes the modal
    var span2 = document.getElementsByClassName("close2")[0];

      var datos2= false;
    modal2.style.display = "none";

    span2.onclick = function() {
      modal2.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal2) {
          modal2.style.display = "none";
      }
  }


  var modal3 = document.getElementById('myModal3');
    var btn3 = document.getElementById("myBtn3");

    // Get the <span> element that closes the modal
    var span3 = document.getElementsByClassName("close3")[0];

      var datos3= false;
    modal3.style.display = "none";

    span3.onclick = function() {
      modal3.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal3) {
          modal3.style.display = "none";
      }
  }

  function openForm()
  {
    document.getElementById('myModal3').style.display='block';

  }

  </script>
<?php 

if (@$info || @$popup)
{
	?>
	<script>

		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>

 