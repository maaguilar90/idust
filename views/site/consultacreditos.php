<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Consulta de Aportaciones y Créditos';
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/javascript">

var estilos='<style type="text/css">body{ font-size:10px; }   thead td{padding: 10px;background: black;text-align: center;padding-bottom: 4px;padding-top: 5px;color: white;width: 10%} tbody td{text-align: center;border: 1px solid black;} table{ width: 100%;font-size:10px;} </style>';



function imprSelec(muestra)
{ var ficha=document.getElementById(muestra);var ventimp=window.open(' ','popimpr');ventimp.document.write(estilos+ficha.innerHTML);ventimp.document.close();ventimp.print();ventimp.close();}
</script>
  <section class="container pos-relative margins-top-pg-interna">
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <div id="aportaciones">
    <h3>Aportaciones</h3>

    	<table>
    		<thead>
    			<tr>
    				<td style="    ">Cédula</td>
    				<td style="width: 25%" >Nombres</td>
    				<td style="width: 6%">Cod. Producto</td>
    				<td style="    ">Producto</td>
    				<td style="    ">Acumulado</td>
    			</tr>
    		</thead>
    		<tbody>
    				
    					<?php foreach ($infoaportaciones as $key => $value) { ?>
    				<tr>
    					
	    				<td style="   "><?=$value['codigo'] ?></td>
	    				<td style="   "><?=$value['nombreUnido'] ?></td>
	    				<td style="   "><a href="javascript:winmodal(<?=$value['codigoProducto'] ?>,<?=$value['codigo'] ?>)" > <?=$value['codigoProducto'] ?></a></td>
	    				<td style="   "><?=$value['nombre'] ?></td>
	    				<td style="   "><?=$value['disponible'] ?></td>
					</tr>
    					<?php } ?>
    				
    		</tbody>

    	</table>
    </div>
<a href="javascript:imprSelec('aportaciones');" style="float: right;background: #006298;color: #FFF;padding: 2px 20px; width: 7%;clear: both;text-align: center;font-size: 1.1em;text-decoration: none; "> Imprimir </a>
	
		<br>
      <div id="creditos">
    	<h3>Créditos</h3>
    
    	<table>
    		<thead>
    			<tr>
    				<td style="    ">Cédula</td>
    				<td style="width: 15%" >Nombres</td>
    				<td style="width: 6%"># Pagaré</td>
    				<td style="    ">Tipo Préstamo</td>
    				<td style="    ">Monto Prestado</td>
    				<td style="    ">Tasa de Interés</td>
    				<td style="    ">Fecha adjudicación</td>
    				<td style="width: 8%">Cuotas</td>
    				<td style="width: 15%">Fecha de Vencimiento</td>
    			</tr>
    		</thead>
    		<tbody>
    				
    					<?php foreach ($infocreditos as $key => $value) { ?>
    				<tr>
    					
	    				<td style="   "><?=$value['Cedula'] ?></td>
	    				<td style="   "><?=$value['Nombre'] ?></td>
	    				<td style="   "><?=$value['NumeroPrestamo'] ?></td>
	    				<td style="   "><?=$value['TipoPrestamo'] ?></td>
	    				<td style="   "><?=$value['MontoPrestado'] ?></td>
              <td style="   "><?=$value['tasaInteres'] ?></td>

	    				<td style="   "><?=substr($value['fechaAdjudicacion'],0,10) ?></td>
	    				<td style="   "><?=$value['numeroCuotas'] ?></td>
	    				<td style="   "><?=substr($value['fechaVencimiento'],0,10) ?></td>
	    			</tr>
    					<?php } ?>
    				
    		</tbody>

    	</table>
      </div>
   <a href="javascript:imprSelec('creditos');" style="float: right;background: #006298;color: #FFF;padding: 2px 20px; width: 7%;clear: both;text-align: center;font-size: 1.1em;text-decoration: none; "> Imprimir </a>

	
		<br>
    <div id="capital">
	   <h3>Capital por Pagar</h3>

      <table>
        <thead>
          <tr>
            <td style="    "># Pagaré</td>
            <td style="width: 15%" >Tipo</td>
            <td style="    ">Deuda Incial</td>
            <td style="    ">Capital Pagado</td>
            <td style="    ">Saldo Capital</td>
          </tr>
        </thead>
        <tbody>
            
              <?php foreach ($infocapital as $key => $value) { ?>
            <tr>
              
              <td style="   "><?=$value['numeroPagare'] ?></td>
              <td style="   "><?=$value['nombre'] ?></td>
                <td style="   "><?=$value['deudaInicial'] ?></td>
              <td style="   "><?=$value['cappagado'] ?></td>
              <td style="   "><?=@$value['deudaInicial'] - @$value['cappagado']  ?></td>
            </tr>
              <?php } ?>
            
        </tbody>

      </table>
      </div>
   <a href="javascript:imprSelec('capital');" style="float: right;background: #006298;color: #FFF;padding: 2px 20px; width: 7%;clear: both;text-align: center;font-size: 1.1em;text-decoration: none; "> Imprimir </a>

  
    <br>
    <div id="cuotasv">
    <h3>Cuotas Vencidas</h3>

      <table>
        <thead>
          <tr>
            <td style="    "># Pagaré</td>
            <td style="width: 15%" >Cuota vencida</td>
      
          </tr>
        </thead>
        <tbody>
            
              <?php foreach ($infocuotas as $key => $value) { ?>
            <tr>
              
              <td style="   "><?=$value['numeroPagare'] ?></td>
              <td style="   "><?php if (!$value['cuotavencida']){ echo '0.00'; }else{ echo $value['cuotavencida']; } ?></td>
            </tr>
              <?php } ?>
            
        </tbody>

      </table>
     </div>
   <a href="javascript:imprSelec('cuotasv');" style="float: right;background: #006298;color: #FFF;padding: 2px 20px; width: 7%;clear: both;text-align: center;font-size: 1.1em;text-decoration: none; "> Imprimir </a>

  
    <br>
  
   <div id="cuotasv">
    <h3>Rol Jubilados Complementarios</h3>
<?= ''//var_dump($inforoles) ?>
      <table style="width: 99.1%;">
          <thead>
            <tr>
              <td style="    "># Secuencial</td>
              <td style="width: 15%" >Año</td>
              <td style="width: 15%" >Mes</td>
              <td style="width: 15%" ></td>
        
            </tr>
          </thead>
      </table>
      <div id="style-1" style="height: 200px; overflow-y: auto; overflow-x: hidden;">
        <table >
          <!--<thead>
            <tr>
              <td style="    "># Secuencial</td>
              <td style="width: 15%" >Año</td>
              <td style="width: 15%" >Mes</td>
              <td style="width: 15%" ></td>
        
            </tr>
          </thead>-->
          <tbody>
              
                <?php foreach ($inforoles as $key => $value) { ?>
              <tr>
                <?php 
                  $nmes='';
                  switch ($value['Mes']) {
                    case '1': $nmes='Enero'; break;
                    case '2': $nmes='Febrero'; break;
                    case '3': $nmes='Marzo'; break;
                    case '4': $nmes='Abril'; break;
                    case '5': $nmes='Mayo'; break;
                    case '6': $nmes='Junio'; break;
                    case '7': $nmes='Julio'; break;
                    case '8': $nmes='Agosto'; break;
                    case '9': $nmes='Septiembre'; break;
                    case '10': $nmes='Octubre'; break;
                    case '11': $nmes='Noviembre'; break;
                    case '12': $nmes='Diciembre'; break;
                    
                    default: $nmes=''; break;
                  }
                ?>
                <td style="   "><?=$value['Secuencial'] ?></td>
                <td style="   "><?=$value['Año'] ?></td>
                <td style="   "><?=$nmes ?></td>
                <td><a href="javascript:winmodalRol(<?=$value['Secuencial'] ?>)" style="margin-top:0px;background: #006298;color: #FFF;padding: 0px 20px 1px 20px; clear: both;text-align: center;font-size: 1.1em;text-decoration: none; "> Ver </a></td>
              </tr>
                <?php } ?>
              
          </tbody>

          </table>
        </div>
    </div>
   

  
    <br>
  
			
    </p>

  
</div>
<!-- Trigger/Open The Modal -->
<!--<button id="myBtn">Open Modal</button>-->

<!-- The Modal -->
<div id="myModal" class="modal"  style="overflow: hidden; z-index: 99999;">

  <!-- Modal content -->
  <div class="modal-content" style="height: 70%;   top: -10%;   overflow-y: auto;">
   <a href="javascript:imprSelec('contentmodal');" style="float: left;background: #006298;color: #FFF;padding: 2px 20px; width: 7%;clear: both;text-align: center;font-size: 1.1em;text-decoration: none; "> Imprimir </a>

    <span class="close">x</span>
    <div id="contentmodal" style="margin-top:30px;">

    </div>
  </div>

</div>

<div id="myModalRol" class="modal" style="overflow: hidden; z-index: 99999;">

  <!-- Modal content -->
  <div class="modal-content" style="height: 90%; width: 50%;    top: -20%;   overflow-y: auto;">
   <a href="javascript:imprSelec('contentmodalRol');" style="background: #006298;color: #FFF;padding: 2px 20px; width: 13%;clear: both;text-align: center;font-size: 1.1em;text-decoration: none; "> Imprimir </a>&nbsp;&nbsp;
<a href="javascript:imprSelec('contentmodalRol');" style="background: #006298;color: #FFF;padding: 2px 20px; width: 13%;clear: both;text-align: center;font-size: 1.1em;text-decoration: none; "> Pdf </a>
    <span class="close">x</span>
    <div id="contentmodalRol" style="margin-top:10px;">

    </div>
  </div>

</div>

</section>
<style type="text/css">
	thead td
	{
		padding: 10px;
	    background: black;
	    text-align: center;
	    padding-bottom: 4px;
	    padding-top: 5px;
	    color: white;
	    width: 10%
	}
	tbody td
	{
			text-align: center;
		    border: 1px solid black;
	}
	table
	{
		width: 100%;
	}
	/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 27px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
#style-1::-webkit-scrollbar
{
  width: 11px;
  background-color: #F5F5F5;
}

#style-1::-webkit-scrollbar-thumb
{
  border-radius: 10px;
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
  background-color: black;
}
</style>
<script type="text/javascript">
	// Get the modal


 function numberFormat(numero){
        // Variable que contendra el resultado final
        var resultado = "";
 
        // Si el numero empieza por el valor "-" (numero negativo)
        if(numero[0]=="-")
        {
            // Cogemos el numero eliminando los posibles puntos que tenga, y sin
            // el signo negativo
            nuevoNumero=numero.replace(/\./g,'').substring(1);
        }else{
            // Cogemos el numero eliminando los posibles puntos que tenga
            nuevoNumero=numero.replace(/\./g,'');
        }
 
        // Si tiene decimales, se los quitamos al numero
        if(numero.indexOf(",")>=0)
            nuevoNumero=nuevoNumero.substring(0,nuevoNumero.indexOf(","));
 
        // Ponemos un punto cada 3 caracteres
        for (var j, i = nuevoNumero.length - 1, j = 0; i >= 0; i--, j++)
            resultado = nuevoNumero.charAt(i) + ((j > 0) && (j % 3 == 0)? ".": "") + resultado;
 
        // Si tiene decimales, se lo añadimos al numero una vez forateado con 
        // los separadores de miles
        if(numero.indexOf(",")>=0)
            resultado+=numero.substring(numero.indexOf(","));
 
        if(numero[0]=="-")
        {
            // Devolvemos el valor añadiendo al inicio el signo negativo
            return "-"+resultado;
        }else{
            return resultado;
        }
    }

function winmodal(idproduct,identity)
{



	console.log(idproduct)
	var modal = document.getElementById('myModal');

	// Get the button that opens the modal
	var btn = document.getElementById("myBtn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on the button, open the modal 
	/*btn.onclick = function() {
	    
	}*/
    var datos= false;
	modal.style.display = "block";
    $('#contentmodal').html('Espere...');
	$.post("../site/consultacreditos",
    {
        identity: identity,
        idproduct: idproduct,
    },
    function(data, status){
        //console.log("Data: " + data + "\nStatus: " + status);
        
       var tablecontent="";
       var initable="";
        var str =data;
        str=str.replace('[', '')
        str=str.replace(/"/g, '')
        str=str.replace(/}]/g, '')
        str=str.replace(/00:00:00/g, '')
        var res=str.split('{');

        for (i = 0; i < res.length; i++) {  
            datos=true;
     		//console.log(res[i]);
			tablecontent+='<tr>';
     		var sep= res[i].split(':');
     		//console.log('------')
     		for (j = 0; j < sep.length; j++) {  

     			value=sep[j].replace(/,codigoProducto/g, '')
     			value=value.replace(/,codigo/g, '')
     			value=value.replace(/numeroCliente/g, '')
     			value=value.replace(/,nombreUnido/g, '')
     			value=value.replace(/,Producto/g, '')
     			value=value.replace(/,nombre/g, '')
     			value=value.replace(/,Valor/g, '')
     			value=value.replace(/,fecha/g, '')
          value=value.replace(/,saldo/g, '')
          value=value.replace(/,CodigoFondo/g, '')
          value=value.replace(/,Nombre/g, '')
          value=value.replace(/,Movimiento/g, '')
          value=value.replace(/,Fondo/g, '')
          value=value.replace(/,cedula/g, '')
     
     			value=value.replace(/},/g, '')
     			value=value.replace(/}],/g, '')
     			if (j==2) {tablecontent+='<td>'+value+'</td>'; }
     			if (j==3) {tablecontent+='<td>'+value+'</td>'; }
     			if (j==4) {tablecontent+='<td>'+value+'</td>'; }
     			if (j==5) {tablecontent+='<td>'+value+'</td>'; }
     			if (j==6) {tablecontent+='<td>'+value+'</td>'; }
          if (j==7) {tablecontent+='<td>'+value+'</td>'; }
     			if (j==8) {tablecontent+='<td>'+value+'</td>'; }
     			if (j==9) {tablecontent+='<td>'+value+'</td>'; }
     			
     			console.log(j+': '+value)
     		}
        	tablecontent+='</tr>';
     		
    	}

        if (datos==false){ $('#contentmodal').html('No se encontraron movimientos.'); } 
		initable="<table><thead><tr><td >Cédula</td><td style='width: 25%' >Nombres</td><td style='width: 6%'>Cod. Producto</td><td>Fondo </td><td>Movimiento </td><td>Fecha</td><td>Valor</td><td>Saldo</td></tr></thead><tbody>"+tablecontent+"</tbody></table>";

		 $('#contentmodal').html(initable);

        //res= res.;
        //console.log(res[0]+res[1])
      //  for (i=0;i<res.length; i+9)
       // {
        	//console.log(res[i]);
        	//break;
       // }
       /*for (nombreObjeto in data) {
       	console.log(nombreObjeto)
}*/
	//console.log()
	/*for (x=0;x<data.length;x++){
       console.log(data[x] + " ");
}*/


    });


	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}

}

function winmodalRol(secuencial)
{



  console.log(secuencial)
  var modal = document.getElementById('myModalRol');

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[1];

  var datos= false;
  modal.style.display = "block";
  $('#contentmodalRol').html('Espere...');
  $.post("../site/consultacreditos  ",
    {
        secuencial: secuencial,
        consulta: 1,
    },
    function(data, status){
        //console.log("Data: " + data + "\nStatus: " + status);
        
       var tablecontent="";
       var initable="";
        var str =data;
        str=str.replace('[', '')
        str=str.replace(/"/g, '')
        str=str.replace(/}]/g, '')
        str=str.replace(/00:00:00/g, '')
        var res=str.split('{');

        for (i = 0; i < res.length; i++) {  
            datos=true;
        console.log(res[i]);
      tablecontent+='<tr>';
        var sep= res[i].split(':');
        //console.log('------')
        for (j = 0; j < sep.length; j++) {  

          value=sep[j].replace(/Secuencial/g, '')
          value=value.replace(/,Anio/g, '')
          value=value.replace(/,Mes/g, '')
          value=value.replace(/,Nombre/g, '')
          value=value.replace(/,Salud/g, '')
          value=value.replace(/,PreQui/g, '')
          value=value.replace(/,PreHip/g, '')
          value=value.replace(/,PreEme/g, '')
          value=value.replace(/,PrePre/g, '')
          value=value.replace(/,TotPrestamos/g, '')
          value=value.replace(/,Transferencia/g, '')
          value=value.replace(/,Tonsupa/g, '')
          value=value.replace(/,OtrosDsctos/g, '')
          value=value.replace(/,TotDsctos/g, '')
          value=value.replace(/,Observacion/g, '')
     
          value=value.replace(/},/g, '')
          value=value.replace(/}],/g, '')

          
          console.log(j+': '+value)
          if (j==1){ var codigo=value; }
          if (j==2){ var anio=value; }
          if (j==4){ var nombres=value; }
          if (j==5){ var salud=value; }
          if (j==6){ var prestamosq=value; }
          if (j==7){ var prestamoship=value; }
          if (j==8){ var prestamoseme=value; }
          if (j==9){ var prestamospre=value; }
          if (j==10){ var totalpres=value; }
          if (j==11){ var transferencia=value; }
          if (j==12){ var totalton=value; }
          if (j==13){ var otrosdes=value; }
          if (j==14){ var totaldes=value; }
          if (j==15){ var observacion=value; }
          if (j==3)
          {
          var mes=value;

          if (mes==1){ mes='Enero'; }
          if (mes==2){ mes='Febrero'; }
          if (mes==3){ mes='Marzo'; }
          if (mes==4){ mes='Abril'; }
          if (mes==5){ mes='Mayo'; }
          if (mes==6){ mes='Junio'; }
          if (mes==7){ mes='Julio'; }
          if (mes==8){ mes='Agosto'; }
          if (mes==9){ mes='Septiemre'; }
          if (mes==10){ mes='Octubre'; }
          if (mes==11){ mes='Noviembre'; }
          if (mes==12){ mes='Diciembre'; }
          }
        }
         
        
      }


      $.post("../site/consultacreditos",
    {
        secuencial: secuencial,
        consulta: 2,
    },
    function(data, status){
        //console.log("Data: " + data + "\nStatus: " + status);
        
       var tablecontent="";
       var initable="";
        var str =data;
        str=str.replace('[', '')
        str=str.replace(/"/g, '')
        str=str.replace(/}]/g, '')
        str=str.replace(/00:00:00/g, '')
        var res=str.split('{');

        for (i = 0; i < res.length; i++) {  
            datos=true;
        //console.log(res[i]);
      tablecontent+='<tr>';
        var sep= res[i].split(':');
        //console.log('------')
        for (j = 0; j < sep.length; j++) {  

          value=sep[j].replace(/Secuencial/g, '')
          value=value.replace(/,Anio/g, '')
          value=value.replace(/,Mes/g, '')
          value=value.replace(/,Nombre/g, '')
          value=value.replace(/,Prension/g, '')
          value=value.replace(/,OtrosIngresos/g, '')
          value=value.replace(/,TotalIngresos/g, '')
     
          value=value.replace(/},/g, '')
          value=value.replace(/}],/g, '')
          
          console.log(j+': '+value)
          var cedula='<?=$cedula?>';
          //var cedula='0000';
          if (j==5){ var pension=value; }
          if (j==6){ var otrosing=value; }
          if (j==7){ var totaling=value; }


        }
         //<tr><td style='padding-right: 1%;'>&nbsp; Préstamos Asoprep:</td><td> "+parseFloat(pension).toFixed(2)+"</td></tr>
         if (datos==false){ $('#contentmodalRol').html('No se encontraron movimientos.'); }
    initable="<table><thead><tr><td style='width: 10%;  background-color: transparent;border: 1px solid black;' ><img src='/web/images/logo-asoprep.svg' alt='Asoprep' class='' style='width: 100%;'></td><td style='width: 90%; background-color: transparent; color: black; vertical-align: middle;border: 1px solid black;'>ASOCIACION DEL FONDO COMPLEMENTARIO PREVISIONAL CERRADO<br>Registro Oficial No.: 453, del 20 de mayo del 2011 <br>Resolución No: SBS-2011-277, del 31 de marzo del 2011</td></tr></thead></table><table><tbody><tr><td>ROL INDIVIDUAL POR PENSION COMPLEMENTARIA<br>AÑO: "+anio+" &nbsp;&nbsp;&nbsp;&nbsp;MES: "+mes+"</td></tr></tbody></table><table><tbody><tr><td style='width: 50%'>Cédula: "+cedula+"</td><td style='width: 50%'>Nombres: "+nombres+"</td></tr></tbody></table>"; 
    initable+="<table><thead><tr><td style='width: 50%'>INGRESOS</td><td style='width: 50%'>DESCUENTOS</td></tr></thead><tbody><tr><td style='width: 50%;text-align: left;'><table id='contentTable'><tr><td style='padding-right: 1%;'>&nbsp; Prensión Complementaria: </td><td>"+parseFloat(pension).toFixed(2)+"</td></tr><tr><td>&nbsp; Otros Ingresos: </td><td>"+parseFloat(otrosing).toFixed(2)+"</td></tr></table></td><td style='width: 50%;text-align: left;'><table id='contentTable'><tr><td style='padding-right: 1%;'>&nbsp; Cobertura Salud:</td><td>"+parseFloat(salud).toFixed(2)+"</td></tr><tr><td style='padding-right: 1%;'>&nbsp; Quirografario: </td><td>"+parseFloat(prestamosq).toFixed(2)+"</td></tr><tr><td style='padding-right: 1%;'> &nbsp;&nbsp;Hipotecario: </td><td>"+parseFloat(prestamoship).toFixed(2)+"</td></tr><tr><td style='padding-right: 1%;'>&nbsp; Emergente: </td><td>"+parseFloat(prestamoseme).toFixed(2)+"</td></tr><tr><td style='padding-right: 1%;border-bottom: none;'>&nbsp; Prendario: "+parseFloat(prestamospre).toFixed(2)+"</td></tr><tr><td></td><td></td></tr><tr><td style='padding-right: 1%;'>&nbsp; Descuento Tonsupa: </td><td>"+parseFloat(totalton).toFixed(2)+"</td></tr><tr><td style='padding-right: 1%;'>&nbsp; Transferencia Bancaria: </td><td  style=''>"+parseFloat(transferencia).toFixed(2)+"</td></tr><tr><td style='padding-right: 1%;border-bottom: none;'> &nbsp;&nbsp;Otros Descuentos: </td><td style='border: none;'>"+parseFloat(otrosdes).toFixed(2)+"</td></tr></table></td></tr></tbody></table><br><table><tbody><tr><td style='width: 50%'>Total Ingresos: "+parseFloat(totaling).toFixed(2)+"</td><td style='width: 50%'>Total Descuentos: "+parseFloat(totaldes).toFixed(2)+"</td></tr></tbody></table><table><tbody><tr><td style='width: 100%; text-align: center;'>Líquido a recibir: "+parseFloat(totaling-totaldes).toFixed(2)+"</td></tr></tbody></table>";

     $('#contentmodalRol').html(initable);

      }


    });

       
    });


  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
      modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }
  }

}


</script>

<style type="text/css">
  #contentTable tr td{
    border: none;
    border-bottom: 1px solid gray;
    text-align: right;
    padding-right: 10%;
  }
</style>