<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Actualización de Clave Personal';
$this->params['breadcrumbs'][] = $this->title;
?>
  <section class="container pos-relative margins-top-pg-interna">
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Por favor ingresa la nueva contraseña:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
            <?= $form->field($model,'password')->input('password')->label('Contraseña.') ?>
            <?= $form->field($model,'confirmpassword')->input('password')->label('Repita su contraseña.') ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
</section>
