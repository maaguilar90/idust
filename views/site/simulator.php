<?php

/* @var $this \yii\web\View */
/* @var $content string 

x=monto*(0.09(1+0.09)elevadoplazo)
*/

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\View;
$script=<<< JS
 var fotorama = $('.fotorama').data('fotorama');
$("#calc").click(function(){
var x=0;
var monto=$("#monto").val();
var plazo=$("#plazo").val();
var taza=0.0075;
x=monto*((taza*Math.pow(taza+1,plazo))/(Math.pow(taza+1,plazo)-1));
x=x.toFixed(2);
console.log(x);
$(".result-simulator").css({opacity:1});
$("#value-simulator").html(x);

  });
JS;
$this->registerJs($script,View::POS_END);
AppAsset::register($this);
$this->title="ASOPREP-FCPC | Créditos";
?>
        <section class="container pos-relative margins-top-pg-interna">
        	<div class="cont-simuladores">
            	<img src="<?= URL::base() ?>/images/ico-simuladorcredito.svg"/>
            	<div class="cont-calcu">
                	<form>
                    	<label>Ingrese su monto:</label>
                        <input id="monto" type="text"/>
                        <label>Plazo en Meses:</label>
                        <input id="plazo" type="text"/>
                       <a href="#" id="calc" >Calcular</a>
                    </form>
                    <div class="result-simulator">
                        SU CUOTA MENSUAL A PAGAR ES: <span id="value-simulator"></span>
                    </div>
                </div>
            </div>
        </section>