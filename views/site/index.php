<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'iDust';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	$('.slider-carousel').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 3,
    responsive: [
    {
    breakpoint: 1024,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
    },
    {
    breakpoint: 600,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 2
    }
    },
    {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });
  

});

$(document).ready(function(){
      $('#carousel').carousel3d();
      document.getElementById("sectour").style.display="none";
    });

JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>



<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


    
<section class="container-fluid">
	<div class="fotorama" data-autoplay="true">
        <?php $sliders = Slider::find()->OrderBy('orden')->all(); ?>
        <?php foreach ($sliders as $slider): ?>

		<!--<div data-img="<?= URL::base() ?>/images/banner/<?= $slider->image ?>"><a href="<?= $slider->description ?>"></a></div>-->
        <?php endforeach ?>


	</div>
  <div class="container-redes">
    <div>
      <a  target="_blank" href="https://www.facebook.com/idustnyc">
      <img src="<?= URL::base() ?>/images/site/facebook_icon.svg" class="redes-lateral">
      </a>
    </div>
    <div>
      <a  target="_blank" href="https://twitter.com/idustnyc">
        <img src="<?= URL::base() ?>/images/site/twitter_icon.svg" class="redes-lateral">
      </a>
    </div>
    <div>
      <a  target="_blank" href="#">
        <img src="<?= URL::base() ?>/images/site/instagram_icon.svg" class="redes-lateral">
      </a>
    </div>
    <div>
      <a  target="_blank" href="#">
        <img src="<?= URL::base() ?>/images/site/whatsapp_icon.svg" class="redes-lateral">
      </a>
    </div>
    <div>
      <a  target="_blank" href="#">
        <img src="<?= URL::base() ?>/images/site/youtube_icon.svg" class="redes-lateral">
      </a>
    </div>
    
  </div>
  <div id="container" style="height: 610px; padding-top: 4%; background-color: #EEE;"  class="fotorama-carousel">
     <div id="perspective">
      <div id="carousel">
        <!--<figure> <img src="<?= URL::base() ?>/images/site/ruleta1.png" alt="fotorama1" style="width: 300px"></figure>-->

        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta01.fw.png" alt="fotorama1" style="width: 300px">
          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta02.fw.png" alt="fotorama1" style="width: 300px">
          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta03.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta04.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta05.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta06.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <!--<div class="div-carousel">
              <span class="span-carousel">We'll fit all your cleaning needs. 100% guarantee on our work</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>-->
            <img src="<?= URL::base() ?>/images/site/ruleta07.fw.png" alt="fotorama1" style="width: 300px">

          </div>
        </figure>
        <figure> 
          <div alt="fotorama1" style="width: 300px" class="carousel-portal">
            <div class="div-carousel">
              <span class="span-carousel">&nbsp</span>
              <div>
                <button class="button-carousel">GET A CUOTE NOW</button>
              </div>
            </div>
            <img src="<?= URL::base() ?>/images/site/ruleta1.png" alt="fotorama1" style="width: 300px">
          </div>
        </figure>
      </div>
    </div>

    <button class="prev"> < </button>
    <button class="next"> > </button>
  </div>

</section>
        <!-- --> 


<section class="container-fluid" style="margin-top: -5.5%; ">
    <div class="servicio" style="background-color: black;">
        <div class="servicio-int" style="border: none;">
          <a href="javascript:seccion(1)">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" id="btn1" class="btn1" alt="servicio1" class="" style="">
          </a>
        </div>
        <div class="servicio-int">
          <a href="javascript:seccion(2)">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" id="btn2" class="btn2" alt="servicio1" class="" style="">
          </a>
        </div>
        <div class="servicio-int">
          <a href="javascript:seccion(3)">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" id="btn3" class="btn3" alt="servicio1" class="" style="">
          </a>
        </div>
        <div class="servicio-int">
          <a href="javascript:seccion(4)">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" id="btn4" class="btn4" alt="servicio1" class="" style="">
          </a>
        </div>
        <div class="servicio-int">
          <a href="javascript:seccion(5)">
             <img src="<?= URL::base() ?>/images/site/botonblank.fw.png" id="btn5" class="btn5" alt="servicio1" class="" style="">
          </a>
        </div>
    </div>
</section>

<section class="container-fluid" id="sectabout">
    <div class="cleaning-services" style="">
      <div class="cleaning-left">
        <div class="bloque">
          <div class="label2">
              <img class="imgsuper" src="<?= URL::base() ?>/images/site/aboutus.jpg"  />
          </div>

          <div class="label1 tjustify">
              <div class="column1 tcenter">
                <span class="title1">ABOUT</span><span class="title2">US</span><br><br>
              </div>
              <span>Founded in the spring of 2014, iDust Cleaning Services Corp is a janitorial company with a reputation for excellence and has been specializing in the Restaurant and Hotel industry sector, from industrial Cleaning, post-construction cleaning, to general office cleaning and Interior/exterior window cleaning all year around.</span><br><br>
              <span style="font-size: 18px; font-weight:bold;">Our mission </span><br>
              <span>iDust’s mission is to provide superior, dependable and professional cleaning services to our clients. <br> We are committed to providing the most professional and thorough janitorial services <br>We maintain an up-front and sustained commitment to the successful progress and ongoing operation of all our projects throughout the city.</span>
          </div>
          
        </div>
        <div class="bloque">
          <div class="label1 tjustify">
              <span>Our dedicated and experienced staff ensures the quality of cleanliness and the appearance and safety of our clients’ facilities at all times.</span><br>
              <span style="font-size: 18px; font-weight:bold;">Our objective </span><br>
              <span>Is to always protect our client’s investment by providing quality work and the appearance and safety of our clients’s facilities at all times even when you are not there, constant communication on matters that need immediate attention.</span><br><br>
              <span style="font-size: 18px; font-weight:bold;">Our principles </span><br>
              <span>Some of our principles are quality customer service so when you call us you will speak directly with the Owner who understands how important your time and business is and will provide immediate attention and prompt compliant resolution.</span><br><br>
              <span style="font-size: 18px; font-weight:bold;">Our Goals </span><br>
              <span>iDust Cleaning Services Corp’s leadership in the Cleaning industry is a direct result of our dedication to customer service without compromise. For each client, we create a safety and implementation plan that is customized to meet the client’s specific business goals and objectives. </span>
          </div>
          <div class="label2 right">
              <img class="imgsuper right" src="<?= URL::base() ?>/images/site/aboutus.jpg"  />
          </div>
        </div>
      </div>
      
    </div>
</section>
<section class="container-fluid"  id="sectwork">
  <div class="workwith" style="">
    <div class="work-left">
      <img class="imgsuper image" src="<?= URL::base() ?>/images/site/imgtrabaja.jpg"  />
    </div>
    <div class="work-right">
      <div class="column1 tcenter">
        <span class="title1">WORK WITH</span><span class="title2">US</span><br><br>
        <br>
        <button class="button">REGISTER NOW</button>
      </div>
    </div>
  </div>
</section>

<section class="container-fluid"  id="sectour">
    <div class="how-works">
      <div class="contenedor">
        <div class="seccion">
          <div class="slider-carousel2" style="">
            <div>
              <img class="img-slider2" src="<?= URL::base() ?>/images/site/sliderwork1.jpg"/>
              <div class="content-slider" style="margin-top: -11%;">
                <div style="text-align: center;">
                  <span class="title">KITCHEN</span><br><br>
                  <span class="text-content">Care on the surfaces of each corner of the kitchen with a special polish for the metal, makes our service the best!<br>
                    <div style="text-align: left; background-color: transparent; padding-top: .5%;">
                      • Kitchen Hood exhausts<br>
                      • Hanging pipes & lights<br>
                      • Hood Filters<br>
                      • Dish pit hoods<br>
                      • Walls <br>
                      • Ceilings<br>
                      • Shelves & fixture cleaning.<br>
                      • Heat lights.<br>
                      <!--• Prep Tables<br>
                      • Sinks<br>
                      • Prep Stations <br>
                      • Faucets all areas washed & sanitized.<br>
                      • Stainless Steel (Cleaned & polished).<br>
                      • All pipes and fixtures <br>
                      • Scrub/ Buff all kinds of Floors.<br>-->
                      • Trash Bins - Emptied, cleaned & sanitized<br>
                      • Storage Areas - Shelving & floors cleaned.<br>
                      </span>
                      </div>
                </div>
              </div>
            </div>
            <div>
              <img class="img-slider2" src="<?= URL::base() ?>/images/site/sliderwork2.jpg"/>
              <div class="content-slider"  style="margin-top: -11%;">
                <div style="text-align: center;">
                  <span class="title">HOTLINE EQUIPEMENT (Equipment Cleaning brushed, & polished)</span><br><br>
                  
                    <span class="text-content">Specialists in high temperature equipment, we make sure that the processes managed and supervised by our team are with the best recommendations of the manufacturers.</span><br>
                    <div style="text-align: left; background-color: transparent; padding-top: .5%;">
                    <span>•  Fryers<br>
                    • Grills<br>
                    • Convection ovens<br>
                    • Deck ovens<br>
                    • Rotisseries<br>
                    • Tilt Skillets<br>
                    • Steam Kettle<br>
                    • Smokers<br>
                  • Griddles<br>
                  • Salamander Broilers and range<br>
                  • Pasta cookers<br>
                  </span>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <img class="img-slider2" src="<?= URL::base() ?>/images/site/sliderwork3.jpg"/>
              <div class="content-slider"  style="margin-top: -11%;">
                <div style="text-align: center;">
                  <span class="title">DINING ROOM - PDR</span><br>
                  
                    <span class="text-content">The details make your place memorable, rely on the total care of your space with the experience of our team</span><br>
                      <div style="text-align: left; background-color: transparent; padding-top: .5%;">
                        <span>
                    • Sweep/Mop floors<br>
                    • Buffer floors<br>
                    • Wax Floors/ Strip Floors<br>
                    • Vacuum carpet<br>
                    • Wipe tables and chair legs <br>
                    • Wipe /vacuum all sitting booths <br>
                    • Wipe down and polish all wood work<br>
                    • Wipe all flat surfaces<br>
                    • Mop all Stairways<br>
                    • Light Fixtures<br>

                  </span>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <img class="img-slider2" src="<?= URL::base() ?>/images/site/sliderwork4.jpg"/>
              <div class="content-slider"  style="margin-top: -10%;">
                <div style="text-align: center;">
                  <span class="title">BAR / NIGHT CLUB</span><br>
                  
                   <span class="text-content">The bar counter will be displayed on your best night with our care.<br></span>
                     <div style="text-align: left; background-color: transparent; padding-top: .5%;">
                      <span>
                    • Bar rails<br>
                    • Bar sinks<br>
                    • Floor drains<br>
                    • Floor mats<br>
                    • Shelves removing all bottles<br>
                    • Pipes<br>
                    • Lowboy equipment? Fridges<br>
                    • Counter tops and stainless steel <br>
                  </span>                
                </div>
                </div>
              </div>
            </div>

            
          </div>
          
         <!-- <div style="text-align: center;">
           
            <span style="color: #b8b8b8; font-size: 20px;">TRIBECA |</span>
            <span style="color: #b8b8b8; font-size: 20px;">NOHO |</span>
            <span style="color: #b8b8b8; font-size: 20px;">GREENWICH VILLAGE |</span>
            <span style="color: #b8b8b8; font-size: 20px;">LOWER EAST |</span>
            <span style="color: #b8b8b8; font-size: 20px;">MIDTOWN EAST</span>
          </div>-->
        </div>
        <div style="clear: both;"></div>
         <div class="items">
              <div class="it-col1">
                <img class="img-item" src="<?= URL::base() ?>/images/site/tiempo.svg"/>
                <br><span>Make an<br> appointment</span>
              </div>
              <div class="it-col2">
                <img class="img-item" src="<?= URL::base() ?>/images/site/localizacion.svg"/>
                <br><span>Review <br>your Place</span>
              </div>
              <div class="it-col3">
                <img class="img-item" src="<?= URL::base() ?>/images/site/telcontact.svg"/>
                <br><span>Contact <br>Us</span>
              </div>
          </div>
      </div>
    </div>
</section>

<section class="container-fluid" id="sectquote" style="display: none;">
    <div class="cleaning-services" style="">
      <div class="cleaning-left">
        <div class="bloque">
          <div class="label1 tjustify" style="vertical-align: middle;">
              <div class="column1 tcenter">
                <span class="title1">GET</span><span class="title2"> A QUOTE</span><br><br>
              </div>
              <table>
                  <tr>
                    <td></td>
                    <td><input type="text" class="forminput" name="" placeholder="Name"></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><input type="text" class="forminput" name="" placeholder="Email"></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><input type="text" class="forminput" name="" placeholder="Phone"></td>
                  </tr>
                  <tr >
                    <td></td>
                    <td><input type="text" class="forminput none" name="" value="Message" readonly="readonly"></td>
                  </tr>
                  <tr>
                    <td style="vertical-align: middle;"></td>
                    <td><textarea style="height: 100px; width: 250px;"></textarea></td>
                  </tr>
                  <tr>
                    <td style="vertical-align: middle;"></td>
                    <td><button class="button">SUBMIT</button></td>
                  </tr>
                </table>
                
          </div>
          <div class="label2 right" style="background-color: #EBCE21;">
              <img class="imgsuper right" src="<?= URL::base() ?>/images/site/aboutus.jpg"  />
          </div>
        </div>
      </div>
      
    </div>
</section>

<section class="container-fluid" id="sectinsurance" style="display: none;">
    <div class="insurance-services" style="">
      <div class="insurance-left">
        <div class="bloque">
          <div class="label1 tjustify">
              <div class="column1 tcenter">
                <span class="title1">INSURANCE</span><span class="title2"></span><br><br>
              </div>
              <span style="font-size: 18px; font-weight:bold;">Safety, Training and Supervision </span><br>
              <span>iDust crews are not just employees but become family so we care for them the same way they care for us and the quality of work we provide and are supervised and supported by an Area Supervisor and also an on-site supervisor that works along with the whole group training the employees on-site at contract start to ensure they understand their tasks and responsibilities, utilize best practices for performance as specified in the contract, and are familiar with building conditions and situations.</span>
          </div>
          <div class="label2 right">
              <img class="imgsuper right" src="<?= URL::base() ?>/images/site/aboutus.jpg"  />
          </div>
          
        </div>
        <div class="bloque">
          <div class="label2">
              <img class="imgsuper" src="<?= URL::base() ?>/images/site/aboutus.jpg"  />
          </div>

          <div class="label1 tjustify">
             
              
              <span>The Area Supervisor will perform site inspections weekly and will monitor performance and employee work habits to the contract and to the quality, safety and implementation plan.</span><br><br>
              <span>Supervisors ensure that employees are taught equipment and chemical safety, safe work procedures and best practices in daily work routines as well as the importance of protecting working area contents and personal belongings. </span><br><br>
              <span>Idust Cleaning Services Corp complies with all relevant Federal, State, and local codes and all other ordinances, regulations, etc. that are applicable to the work. Additionally, <br>we are fully insured, with a combined $5 million General Liability umbrella policy. </span>
          </div>
          
        </div>
      </div>
      
    </div>
</section>

<section class="container-fluid" id="sectcontact" style="display: none;">
    <div class="contact-services" style="">
      <div class="contact-left">
        <div class="bloque">
          <div class="label2 right">
              <img class="imgsuper right" src="<?= URL::base() ?>/images/site/contactus.fw.png"  />
          </div>
          
        </div>
        <div class="bloque">
          <div class="label1 tjustify">
             <div class="column1 tcenter">
                <div style="text-align: left;">
                  <span class="title1">CONTACT</span><span class="title2">US</span><br><br>
                </div>
                <div class="col1">
                  <div style="vertical-align: middle;">
                    <img style="width: 4.8%;" src="<?= URL::base() ?>/images/site/horario.svg"/>
                    <span class="titleencabezado" style="vertical-align: middle;">Schedule Office:</span>
                  </div>
                  <span style="">Mon - Sat: 09:00 - 17:00</span><br>
                  <div style="vertical-align: middle;">
                    <img style="width: 4.8%;" src="<?= URL::base() ?>/images/site/horario.svg"/>
                    <span class="titleencabezado" style="vertical-align: middle;">Schedule Service:</span>
                  </div>
                  <span style="">Mon - Sun: 23:30 - 08:00</span><br>
                </div>
                <div class="col2">
                  <div style="vertical-align: middle;">
                    <img style="width: 4.8%;" src="<?= URL::base() ?>/images/site/ubicacion.svg"/>
                    <span class="titleencabezado" style="vertical-align: middle;">Phone:</span>
                  </div>
                  <span style="">(347) 403-2548</span><br>
                </div>
                <div class="col3">
                  <div class="tleft">
                    <div style="vertical-align: middle;">
                        <img style="width: 4.8%;" src="<?= URL::base() ?>/images/site/ubicacion.svg"/>
                        <span class="titleencabezado" style="vertical-align: middle;">Address:</alto dustinspan>
                    </div>
                    <span style="padding: 0px;">507 W 186th ST New York, NY, 10033</span><br>
                  </div>
                </div>
                <div class="col4">
                  <div class="tcenter">
                    <div style="vertical-align: middle;">
                        <span class="titleencabezado" style="vertical-align: middle;">Email:</span>
                    </div>
                    <span style="">info@idustnyc.com</span><br>
                  </div>
                </div>
              </div>
          </div>
          
        </div>
      </div>
      
    </div>
</section>
<?php $popups = Popup::find()->all(); ?>
        <?php foreach ($popups as $popup): ?>
        <?php $popup=$popup->description ?>
        <?php $tipo=$popup->description ?>
        <?php 
            $contenido='<img src="'.$popup.'" width="98%" />'  

        ?>
        <?php endforeach ?>


<?php 
    if(@$info){
?>
    <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content" style="height: 23%;    overflow-y: auto;">
   
    <span class="close">x</span>
    <div id="contentmodal" style="margin-top:30px;">
        <?=@$infocontent ?>
    </div>
  </div>

</div>
<?php

    }else{
         if(@$popup){
?>
    <div id="myModal" class="modal" style="    overflow-y: hidden;    z-index: 2000;">

  <!-- Modal content -->
  <div class="modal-content" style="    z-index: 2000; height: 85;    overflow-y: auto;     width: 35%;     padding: 2px;    margin: 2% auto;">
   
    <span class="close">x</span>
    <div id="contentmodal" style="margin-top:10px;">
        <?php
           
                echo $contenido;
            
        ?>
        
    </div>
  </div>

</div>
<?php
        }
    }
?>



<script type="text/javascript">
  function seccion(id)
  {
    document.getElementById("btn1").style="background-color: transparent;";
    document.getElementById("btn2").style="background-color: transparent;";
    document.getElementById("btn3").style="background-color: transparent;";
    document.getElementById("btn4").style="background-color: transparent;";
    document.getElementById("btn5").style="background-color: transparent;";
    document.getElementById("btn"+id).style="background-color: #C12A21;";
    if (id==1)
    {
      $("#sectinsurance").hide();
      $("#sectquote").hide();
      $("#sectour").hide();
      $("#sectabout").fadeIn();
      $("#sectwork").fadeIn();
      $("#sectcontact").hide();
    }
    if (id==2)
    {
      $("#sectinsurance").hide();
      $("#sectour").fadeIn();
      $("#sectquote").hide();
      $("#sectabout").hide();
      $("#sectwork").hide();
      $("#sectcontact").hide();
      $('.slider-carousel2').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
    {
    breakpoint: 1024,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      dots: true
    }
    },
    {
    breakpoint: 600,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
    },
    {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });


    }
    if (id==3)
    {
      $("#sectour").hide();
      $("#sectquote").hide();
      $("#sectinsurance").fadeIn();
      $("#sectabout").hide();
      $("#sectwork").hide();
      $("#sectcontact").hide();
    }
    if (id==4)
    {
      $("#sectinsurance").hide();
      $("#sectour").hide();
      $("#sectquote").fadeIn();
      $("#sectabout").hide();
      $("#sectwork").hide();
      $("#sectcontact").hide();
    }

    if (id==5)
    {
      $("#sectcontact").fadeIn();
      $("#sectinsurance").hide();
      $("#sectour").hide();
      $("#sectquote").hide();
      $("#sectabout").hide();
      $("#sectwork").hide();
    }


  }


</script>



  <script type="text/javascript">

    function openPopup(id)
    {
      var title="";
      var image="";
      var description="";
      if (id==1){
        title="KITCHEN";
        image="<?= URL::base() ?>/images/site/sliderwork1.jpg";
        description="Care on the surfaces of each corner of the kitchen with a special polish for the metal, makes our service the best!";
      }

      if (id==2){
        title="HOTLINE EQUIPEMENT";
        image="<?= URL::base() ?>/images/site/sliderwork2.jpg";
        description="Specialists in high temperature equipment, we make sure that the processes managed and supervised by our team are with the best recommendations of the manufacturers.";
      }

      if (id==3){
        title="DINING ROOM";
        image="<?= URL::base() ?>/images/site/sliderwork7.jpg";
        description="The details make your place memorable, rely on the total care of your space with the experience of our team.";
      }

      if (id==4){
        title="HIGH DUSTIN";
        image="<?= URL::base() ?>/images/site/sliderwork6.jpg";
        description="it appears everywhere but we can keep its place without it.";
      }

      if (id==5){
        title="BAR / NIGHT CLUB";
        image="<?= URL::base() ?>/images/site/sliderwork6.jpg";
        description="The bar counter will be displayed on your best night with our care..";
      }      
      document.getElementById("titlepopup").innerHTML =title;
      document.getElementById("imagepopup").src=image;
      document.getElementById("contentpopup").innerHTML =description;

      var modal2 = document.getElementById('myModal2');
      modal2.style.display = "block";
    }


    //document.getElementById('myModal2').style.display="block"; 
    var modal2 = document.getElementById('myModal2');
    var btn2 = document.getElementById("myBtn2");

    // Get the <span> element that closes the modal
    var span2 = document.getElementsByClassName("close2")[0];

      var datos2= false;
    /*modal2.style.display = "none";*/

    /*span2.onclick = function() {
      modal2.style.display = "none";
  }*/

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal2) {
          modal2.style.display = "none";
      }
  }


  var modal3 = document.getElementById('myModal3');
    var btn3 = document.getElementById("myBtn3");

    // Get the <span> element that closes the modal
    var span3 = document.getElementsByClassName("close3")[0];

     /* var datos3= false;
    modal3.style.display = "none";*/

   /* span3.onclick = function() {
      modal3.style.display = "none";
  }*/

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal3) {
          modal3.style.display = "none";
      }
  }

  function openForm()
  {
    document.getElementById('myModal3').style.display='block';

  }

  </script>
<?php 

if (@$info || @$popup)
{
	?>
	<script>

		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>

 