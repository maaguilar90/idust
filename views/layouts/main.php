<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Section;
AppAsset::register($this);
$sections=Section::find()->where(['status'=>'ACTIVE','section_id'=>NULL])->all();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta name="title" content="Dust">
<meta name="description" content="Dust">
<meta name="google" content="">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>



</head>
<body>
<?php $this->beginBody() ?>

    <div id="cont-general">
        <header class="container-fluid pos-relative">
        <div style="width: 100%; text-align: center;">
          <!--<img src="<?= URL::base() ?>/images/site/cabecera.fw.png" alt="iDust info cabecera" class="img-cabecera" style=" ">-->
          <div style="height: 0px;"></div>
          <a href="<?= URL::base() ?>"><img src="<?= URL::base() ?>/images/site/cabecera-logo.fw.png" alt="iDust Cabecera"  class="img-cabecera" style=""></a>
        </div>

            <!-- -->
            <nav class="navbar navbar-default" role="navigation" style="position: initial;">
              <!-- El logotipo y el icono que despliega el menú se agrupan
                   para mostrarlos mejor en los dispositivos móviles -->
              <div class="navbar-header">
                <div class="menu-principal">
                <ul class="nav navbar-nav" style="float:initial">
                    <div class="bloque-menu tleft">
                      <img src="<?= URL::base() ?>/images/site/correo.svg" alt="iDust Cabecera"  class="" style="">
                      <span>&nbsp;&nbsp;info@idustnyc.com</span>
                    </div>
                    <div class="bloque-menu tright">
                      <img src="<?= URL::base() ?>/images/site/whatsapp.svg" alt="iDust Cabecera"  class="" style="">
                      <span>&nbsp;&nbsp;(347) 403-2548</span>
                    </div>
                     <span class="icon-bar"></span>
            

             
                               
                </ul>
              </div>
                <!--<button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-ex1-collapse">
                  <!--<span class="sr-only">Desplegar navegación</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>-->
                <!--</button>-->
              </div>
             
              <!-- Agrupar los enlaces de navegación, los formularios y cualquier
                   otro elemento que se pueda ocultar al minimizar la barra -->
              <div class="collapse navbar-collapse navbar-ex1-collapse container menu-principal">
                <ul class="nav navbar-nav" style="float:initial">
                    <div class="bloque-menu tleft" style="">&nbsp;</div>
                    <div class="bloque-menu tcenter">
                      <img src="<?= URL::base() ?>/images/site/correo.svg" alt="iDust Cabecera"  class="" style="">
                      <span>&nbsp;&nbsp;info@idustnyc.com</span>
                    </div>
                    <div class="bloque-menu tright">
                      <img src="<?= URL::base() ?>/images/site/whatsapp.svg" alt="iDust Cabecera"  class="" style="">
                      <span>&nbsp;&nbsp;(347) 403-2548</span>
                    </div>
                     <span class="icon-bar"></span>
            

             
                               
                </ul>
              </div>
            </nav>
            <!-- -->
        </header>


    <p id="test" style="display: none;"> </p>

        <?= $content ?>
     
        <footer class="container-fluid background-footer">
            <div class="container">
                <div class="redes">
                   <div style="text-align: center;">
              
                      <a  target="_blank" href="https://twitter.com/idustnyc" style="float: none;">
                        <img class="redes-sociales" src="<?= URL::base() ?>/images/site/twitter.fw.png">
                      </a>&nbsp;&nbsp;
                      <a  target="_blank" href="https://www.facebook.com/idustnyc/" style="float: none;">
                        <img class="redes-sociales" src="<?= URL::base() ?>/images/site/facebook.fw.png">
                      </a>
      
                      <a target="_blank"  href="https://www.linkedin.com/in/administrator-idust-405386155/" style="float: none;">
                        <img class="redes-sociales" src="<?= URL::base() ?>/images/site/in.fw.png">
                      </a>

                      <a target="_blank"  href="#" style="float: none;">
                        <img class="redes-sociales" src="<?= URL::base() ?>/images/site/youtube_white.png">
                      </a>

                      <a target="_blank"  href="#" style="float: none;">
                        <img class="redes-sociales" src="<?= URL::base() ?>/images/site/whats_icon.fw.png">
                      </a>
                      
                    </div>
                </div>
                <div class="col-md-15 col-sm-4 tjustify" style="vertical-align: middle;">
                    <span class="footer-text" style="vertical-align: middle;">As we've been working in this industry for more than decade, we can assure yo that our crew if cable of cleaning absolutely any kind dof a mess.</span>
                </div>
                <div class="col-md-15 col-sm-4 tleft">
                  <div>
                     <span class="footer-textbold orange pcenter">Cleaning Services</span><br>
                    </div>
                    <div>
                     <span class="footer-text pcenter">Kitchen</span><br>
                     <span class="footer-text pcenter">Hotline Equipement</span><br>
                     <span class="footer-text pcenter">Dining Room</span><br>
                     <span class="footer-text pcenter">High Dustin</span><br>
                     <span class="footer-text pcenter">Bar / Night Club</span><br>
                    </div><br>
                </div>
                <div class="col-md-15 col-sm-4 tleft">
                   <span class="footer-textbold orange pcenter">Quick Links</span><br>
                    <a href="javascript:seccion(1)" class="footer-text pcenter">About US</a><br>
                    <a href="javascript:seccion(2)" class="footer-text pcenter">Cleaning Service</a>
                    <a href="javascript:seccion(3)" class="footer-text pcenter">Insurance</a><br>
                    <a href="javascript:seccion(4)" class="footer-text pcenter">Get a Quote</a><br>
                    <a href="javascript:seccion(5)" class="footer-text orange pcenter">Contact US</a>
                </div>
            </div>
            
        </footer>
        <div class="container-fluid" style="background-color: #F5D800; font-family: 'monserrate-light'; text-align: center;color: black; padding: 1%; font-size: 11px; font-weight: bold;">
                2018 iDust. Development BY Acep Sistemas.
            </div>
    </div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
