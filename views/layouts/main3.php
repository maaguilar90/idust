<?php
use yii\helpers\Html;
use yii\web\view;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
$script=<<< JS
 

JS;
$this->registerJs($script,View::POS_END);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="contigo">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= URL::base() ?>/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= URL::base() ?>/images/favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="<?= URL::base() ?>/js/jquery-1.12.3.min.js"></script>
    <script src="<?= URL::base() ?>/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>
</head>
<?php if(isset(Yii::$app->view->params['body'])){ ?>
<body id="<?= Yii::$app->view->params['body'] ?>">
<?php }else{ ?>
<body>
<?php } ?>
<?php $this->beginBody() ?>
<script type="text/javascript">
var contigo = angular.module('contigo', []);
contigo.controller('CarController', function CarController($scope, $rootScope, $window, $http, $location, $element) {
  $scope.car = [];
  $scope.n = 0;
  init();
  $rootScope.$on('addCar', function (event, data) {
    // 'Emit!'
    $scope.car = data;
    $scope.n = ($scope.car)?$scope.car.length:0;
  });
  function init(){
    $scope.car = (localStorage.getItem('cotizacion'))?JSON.parse(localStorage.getItem('cotizacion')):null;
    $scope.n = ($scope.car)?$scope.car.length:0;
  }

});
</script>
<header>
   

</header>

<?= $content; ?>
<!--<footer class="container-fluid text-center">
® <?= date('Y') ?> Continental.   Todos los Derechos Reservados.    Desarrollado por SHARE DIGITAL AGENCY
</footer>-->
<?php $this->endBody() ?>
<div id="menu-principal">
<?= $this->render('@app/views/site/index.php') ?>
</div>
</body>
</html>
<?php $this->endPage() ?>
