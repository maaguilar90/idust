<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;
use yii\helpers\Url;
use machour\yii2\notifications\widgets\NotificationsWidget;

/* @var $this \yii\web\View */
/* @var $content string */

AdminAsset::register($this);
//+


// // You may also use the following static methods to set the notification type:
// Notification::warning(Notification::KEY_NEW_MESSAGE, 1, 1);
// Notification::error(Notification::KEY_NO_DISK_SPACE,'KEY_NO_DISK_SPACE', 1);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= URL::base() ?>/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= URL::base() ?>/images/favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Contigo',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    /*echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/admin/default/index']],
            ['label' => 'Usuarios', 'url' => ['/admin/user/index']],
            ['label' => 'Noticias', 'url' => ['/admin/news/index']],

            ['label' => 'Categorias', 'url' => ['/admin/category/index']],
            ['label' => 'Módulos', 'url' => ['/admin/module/index']],
            ['label' => 'Exámenes', 'url' => ['/admin/test/index']],
            ['label' => 'Preguntas', 'url' => ['/admin/question/index']],
            ['label' => 'Respuestas', 'url' => ['/admin/answer/index']],
            ['label' => 'Marcas', 'url' => ['/admin/brand/index']],
            ['label' => 'Segmentos', 'url' => ['/admin/segment/index']],
            ['label' => 'Productos', 'url' => ['/admin/product/index']],
            ['label' => 'Tecnologías', 'url' => ['/admin/technology/index']],
            ['label' => 'Características', 'url' => ['/admin/characteristic/index']],
            ['label' => 'Beneficios', 'url' => ['/admin/benefit/index']],
            ['label' => 'Íconos', 'url' => ['/admin/icon/index']],
            ['label' => 'Databook', 'url' => ['/admin/databook/index']],
            ['label' => 'Tire Selector Book', 'url' => ['/admin/tireselectorbook/index']],
            ['label' => 'Vehículos Version', 'url' => ['/admin/vehicleversion/index']],
            ['label' => 'Vehículos Marcas', 'url' => ['/admin/vehiclebrand/index']],
            ['label' => 'Vehículos Modelos', 'url' => ['/admin/vehiclemodel/index']],
            ['label' => 'Cotizaciones', 'url' => ['/admin/cotizador/index']],
            ['label' => 'Parámetros', 'url' => ['/admin/params/index']],
            ['label' => 'Seg. Usuario', 'url' => ['/admin/seguimiento/index']],
            ['label' => 'Seg. Módulos', 'url' => ['/admin/segmodulo/index']],
            ['label' => 'Seg. Exámen', 'url' => ['/admin/segexamenes/index']],
            ['label' => 'Seg. Login', 'url' => ['/admin/segingreso/index']],
            ['label' => 'Seg. Actividades', 'url' => ['/admin/segusuario/index']],

            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/site/login']] :
                ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']],
        ],
    ]);*/
    if (Yii::$app->user->identity->type == 'ADMIN') {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'MENÚ',
                    'items' => [
                        ['label' => 'Home', 'url' => ['/admin/default/index']],
                        ['label' => 'Usuarios', 'url' => ['/admin/user/index']],
                        ['label' => 'Noticias', 'url' => ['/admin/news/index']],
                        ['label' => 'Categorias', 'url' => ['/admin/category/index']],
                        ['label' => 'Módulos', 'url' => ['/admin/module/index']],
                        ['label' => 'Exámenes', 'url' => ['/admin/test/index']],
                        ['label' => 'Preguntas', 'url' => ['/admin/question/index']],
                        ['label' => 'Respuestas', 'url' => ['/admin/answer/index']],
                        ['label' => 'Marcas', 'url' => ['/admin/brand/index']],
                        ['label' => 'Segmentos', 'url' => ['/admin/segment/index']],
                        ['label' => 'Productos', 'url' => ['/admin/product/index']],
                        ['label' => 'Tecnologías', 'url' => ['/admin/technology/index']],
                        ['label' => 'Características', 'url' => ['/admin/characteristic/index']],
                        ['label' => 'Beneficios', 'url' => ['/admin/benefit/index']],
                        ['label' => 'Íconos', 'url' => ['/admin/icon/index']],
                        ['label' => 'Databook', 'url' => ['/admin/databook/index']],
                        ['label' => 'Tire Selector Book', 'url' => ['/admin/tireselectorbook/index']],

                        /*['label' => 'Vehículos Version', 'url' => ['/admin/vehicleversion/index']],
                        ['label' => 'Vehículos Marcas', 'url' => ['/admin/vehiclebrand/index']],
                        ['label' => 'Vehículos Modelos', 'url' => ['/admin/vehiclemodel/index']],*/

                        //['label' => 'Cotizaciones', 'url' => ['/admin/cotizador/index']],
                        ['label' => 'Parámetros', 'url' => ['/admin/params/index']],

                        /*['label' => 'Seg. Usuario', 'url' => ['/admin/seguimiento/index']],
                        ['label' => 'Seg. Módulos', 'url' => ['/admin/segmodulo/index']],
                        ['label' => 'Seg. Exámen', 'url' => ['/admin/segexamenes/index']],
                        ['label' => 'Seg. Login', 'url' => ['/admin/segingreso/index']],
                        ['label' => 'Seg. Actividades', 'url' => ['/admin/segusuario/index']],*/

                        ['label' => 'Cotizaciones', 'url' => ['/admin/invoice/index']],
                    ]
                ],
                [
                    'label' => 'Vehículos',
                    'items' => [
                        ['label' => 'Vehículos Version', 'url' => ['/admin/vehicleversion/index']],
                        ['label' => 'Vehículos Marcas', 'url' => ['/admin/vehiclebrand/index']],
                        ['label' => 'Vehículos Modelos', 'url' => ['/admin/vehiclemodel/index']],
                    ]
                ],
                [
                    'label' => 'Seguimientos',
                    'items' => [
                        ['label' => 'Seg. Usuario', 'url' => ['/admin/seguimiento/index']],
                        ['label' => 'Seg. Módulos', 'url' => ['/admin/segmodulo/index']],
                        ['label' => 'Seg. Exámen', 'url' => ['/admin/segexamenes/index']],
                        ['label' => 'Seg. Login', 'url' => ['/admin/segingreso/index']],
                        ['label' => 'Seg. Actividades', 'url' => ['/admin/segusuario/index']],
                    ]
                ],


                Yii::$app->user->isGuest ?
                    ['label' => 'Login', 'url' => ['/site/login']] :
                    ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']],
            ],
        ]);
    }
    if (Yii::$app->user->identity->type == 'CLIENT' OR Yii::$app->user->identity->type == 'COTIZADOR') {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Cotizaciones',
                    'url' => ['/admin/invoice/index']
                ],
                Yii::$app->user->isGuest ?
                    ['label' => 'Login', 'url' => ['/site/login']] :
                    ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']],
            ],
        ]);
    }
    NavBar::end();
    ?>

    <div class="container" style="/*margin-top:8%*/">
        <?php
        NotificationsWidget::widget([
            'theme' => NotificationsWidget::THEME_TOASTR,
            'clientOptions' => [
                'location' => 'es',
            ],
            'counters' => [
                '.notifications-header-count',
                '.notifications-icon-count'
            ],
            'listSelector' => '#notifications',
        ]);
        ?>
        <li class="dropdown notifications-menu" style="    margin-top: 0.7%;
    float: right;
    margin-right: 10%;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning notifications-icon-count">0</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have <span class="notifications-header-count">0</span> notifications</li>
                <li>
                    <div id="notifications"></div>
                </li>
            </ul>
        </li>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ContiGo <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
