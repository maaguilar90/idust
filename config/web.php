<?php

$params = require(__DIR__ . '/params.php');
$config = [
    'id' => 'basic',
    'language'=>'es',
    'timeZone' => 'America/Guayaquil',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
             'layout'=>'column2',
             //'layoutPath' => 'app/themes/adminLTE/layouts',
            /*'view' => [
            'theme' => [
                'pathMap' => ['@app/admin/views' => '@app/themes/adminLTE'],
                'baseUrl' => '@web/admin/index',
            ],
        ],*/
        ]
    ],
    //'layout' => 'column2',
    //'layoutPath' => '@app/themes/adminLTE/layouts',
    'components' => [
        
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'RCBBpJNAIkUtCq8FzvZvdzsp-EJ6IKvl',
            //'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
       'urlManager' => [
          'showScriptName' => false,
          'enablePrettyUrl' => true
        ], 
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];



    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [ //here
            'crud' => [ // generator name
                'class' => 'yii\gii\generators\crud\Generator', // generator class
                'templates' => [ //setting for out templates
                    'custom' => '@vendor/bmsrox/yii-adminlte-crud-template', // template name => path to template
                ]
            ]
        ],
    ];
}

return $config;
